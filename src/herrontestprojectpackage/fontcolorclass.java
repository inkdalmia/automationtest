package herrontestprojectpackage;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class fontcolorclass {
	
	static String hex, bghex;
	static String color, bgcolor;
	
	public static void tagInput(String tag) throws Exception {

        switch (tag) {
	    	case "tag":
	    		List<WebElement> h3 = webutils.driver.findElements(By.tagName("tag"));
	            System.out.println("h3 tag");
	            
	            for (int i = 0; i<h3.size(); i=i+1)
	            {
	            	color = h3.get(i).getCssValue("color");
	            	hex = webutils.rgbtoHex(color);
	            	//System.out.println("Text: " +h3.get(i).getText()+ ", Font size:" +h3.get(i).getCssValue("font-size")+ ", Font family:" +h3.get(i).getCssValue("font-family")+ ", Color:" +hex);
	            	System.out.println("Text: " +h3.get(i).getText()+ ", Font family:" +h3.get(i).getCssValue("font-family")+ ", Color:" +hex);
	            }
	            System.out.println();
	            break;
	    
	    	case "h2":
	    		List<WebElement> h2 = webutils.driver.findElements(By.tagName("h2"));
	            System.out.println("h2 tag");
	       	 
	            for (int i = 0; i<h2.size(); i=i+1)
	            {
	            	color = h2.get(i).getCssValue("color");
	            	hex = webutils.rgbtoHex(color);
	               	System.out.println("Text: " +h2.get(i).getText()+ ", Font family:" +h2.get(i).getCssValue("font-family")+ ", Color:" +hex);
	            }
                System.out.println();
	            break;
	    	case "label":
	    		List<WebElement> label = webutils.driver.findElements(By.tagName("label"));
		        System.out.println("label tag");
		   	 
		        for (int i = 0; i<label.size(); i=i+1)
		        {
	            	color = label.get(i).getCssValue("color");
	            	hex = webutils.rgbtoHex(color);
		        	System.out.println("Text: " +label.get(i).getText()+ ", Font family:" +label.get(i).getCssValue("font-family")+ ", Color:" +hex);
		        }
                System.out.println();
		        break;
	    	case "span":
				List<WebElement> span = webutils.driver.findElements(By.tagName("span"));
		        System.out.println("span tag");
		   	 
		        for (int i = 0; i<span.size(); i=i+1)
		        {
	            	color = span.get(i).getCssValue("color");
	            	hex = webutils.rgbtoHex(color);
		        	System.out.println("Text: " +span.get(i).getText()+ ", Font family:" +span.get(i).getCssValue("font-family")+ ", Color:" +hex);
		        }
                System.out.println();
		        break;
	    	case "a":
				List<WebElement> a = webutils.driver.findElements(By.tagName("a"));
		        System.out.println("a tag");
		   	 
		        for (int i = 0; i<a.size(); i=i+1)
		        {
	            	color = a.get(i).getCssValue("color");
	            	hex = webutils.rgbtoHex(color);
		        	System.out.println("Text: " +a.get(i).getText()+ ", Font family:" +a.get(i).getCssValue("font-family")+ ", Color:" +hex);
		        }
                System.out.println();
		        break;
	    	case "input":
				List<WebElement> input = webutils.driver.findElements(By.tagName("input"));
		        System.out.println("input tag");
		   	 
		        for (int i = 0; i<input.size(); i=i+1)
		        {
	            	color = input.get(i).getCssValue("color");
	            	hex = webutils.rgbtoHex(color);
		        	System.out.println("Text: " +input.get(i).getText()+ ", Font family:" +input.get(i).getCssValue("font-family")+ ", Color:" +hex);
		        }
                System.out.println();
		        break;
	    	case "script":
				List<WebElement> script = webutils.driver.findElements(By.tagName("script"));
		        System.out.println("script tag");
		   	 
		        for (int i = 0; i<script.size(); i=i+1)
		        {
	            	color = script.get(i).getCssValue("color");
	            	hex = webutils.rgbtoHex(color);
		        	System.out.println("Text: " +script.get(i).getText()+ ", Font family:" +script.get(i).getCssValue("font-family")+ ", Color:" +hex);
		        }
                System.out.println();
		        break;
	    	case "img":
				List<WebElement> img = webutils.driver.findElements(By.tagName("img"));
		        System.out.println("img tag");
		   	 
		        for (int i = 0; i<img.size(); i=i+1)
		        {
	            	color = img.get(i).getCssValue("color");
	            	hex = webutils.rgbtoHex(color);
		        	System.out.println("Text: " +img.get(i).getText()+ ", Font family:" +img.get(i).getCssValue("font-family")+ ", Color:" +hex);
		        }
                System.out.println();
		        break;
	    	case "form":
				List<WebElement> form = webutils.driver.findElements(By.tagName("form"));
		        System.out.println("form tag");
		   	 
		        for (int i = 0; i<form.size(); i=i+1)
		        {
	            	color = form.get(i).getCssValue("color");
	            	hex = webutils.rgbtoHex(color);
		        	System.out.println("Text: " +form.get(i).getText()+ ", Font family:" +form.get(i).getCssValue("font-family")+ ", Color:" +hex);
		        }
                System.out.println();
		        break;
	    	case "svg":
				List<WebElement> svg = webutils.driver.findElements(By.tagName("svg"));
		        System.out.println("svg tag");
		   	 
		        for (int i = 0; i<svg.size(); i=i+1)
		        {
	            	color = svg.get(i).getCssValue("color");
	            	hex = webutils.rgbtoHex(color);
		        	System.out.println("Text: " +svg.get(i).getText()+ ", Font family:" +svg.get(i).getCssValue("font-family")+ ", Color:" +hex);
		        }
                System.out.println();
		        break;
	    	case "button":
				List<WebElement> button = webutils.driver.findElements(By.tagName("button"));
		        System.out.println("button tag");
		   	 
		        for (int i = 0; i<button.size(); i=i+1)
		        {
	            	color = button.get(i).getCssValue("color");
	            	hex = webutils.rgbtoHex(color);
	            	bgcolor = button.get(i).getCssValue("background-color");
	            	bghex = webutils.rgbtoHex(bgcolor);
            		System.out.println("Text: " +button.get(i).getText()+ ", Font family:" +button.get(i).getCssValue("font-family")+ ", Color:" +hex+ ", Background Color:" +bghex);
		        }
                System.out.println();
		        break;
		     default:
		        	break;
        }
	}
	
	public static void runningAllTags() throws Exception {
		
		tagInput("h2");
		tagInput("h3");
		tagInput("label");
		tagInput("span");
		tagInput("a");
		tagInput("input");
		tagInput("script");
		tagInput("img");
		tagInput("div");
		tagInput("form");
		tagInput("svg");
	}

}
