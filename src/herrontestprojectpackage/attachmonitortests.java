package herrontestprojectpackage;

import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

public class attachmonitortests {
	
	String tempname = "TestTemplate", make = "Make1", model = "Model1", sensitivity = "100", bias = "100";
	String plant = "Test3", building = "Building3", equip = "Equip3", monitor = "PR Mon3";
	WebElement ele;
	
	@Test(priority=0)
	public void initialstep() throws Exception {
			
		webutils.launchFFBrowser();

		webutils.navigatetoURL();
			
		webutils.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		webutils.driver.manage().timeouts().setScriptTimeout(120, TimeUnit.SECONDS);
			
		webutils.signinAs("admin", "pass@123");
		Assert.assertEquals("Dashboard", webutils.driver.getTitle());
	}
	
	@Test(priority=1)
	public void createSensor() throws Exception {
		
		herrontestclass.gotoSensorTemplate();
		herrontestclass.sensorTemplate( make, model, sensitivity, bias);
		Assert.assertEquals(true, webutils.driver.getPageSource().contains("Sensor template added successfully"));
	}
	
	@Test(priority=2)
	public void attachMonitor() throws Exception {
		
		herrontestclass.gotoEquipmentSetup();
		
		herrontestclass.plantNavigation(plant, building, equip);
		
		herrontestclass.attachMonitor(monitor);
		/**
		String rownum = herrontestclass.sensormaptableStartLocation();

		herrontestclass.sensorMapTable(rownum, "1", "V", "S51", tempname, "100", "51", "0.01", "0.25");
		
		int irownum =Integer.parseInt(rownum);	
		rownum = String.format("%01d",irownum + 1);
		
		herrontestclass.sensorMapTable(rownum, "2", "H", "S52", tempname, "100", "52", "0.01", "0.25");
		**/
		int rownum = 0;
		
		herrontestclass.newUIsensorMapTable(rownum, "2", "ACC Sensor", "2", "2", "2", "0.01", "0.25", "2", "V", "A2");
		
		herrontestclass.sensormapSave();
		ele = webutils.driver.findElement(By.xpath("//*[@id='messages_container']/div/div/div/span"));
		Assert.assertEquals("Sensor channel mapping done successfully.", ele.getText());
	}
	
	@Test(priority=3)
	public void logout() throws Exception {
		
		webutils.logOut();
		Assert.assertEquals("Stafford Herron", webutils.driver.getTitle());
	}
	
	@AfterTest
	public void shutdown() throws Exception {
			
		webutils.terminate();
	}
}
