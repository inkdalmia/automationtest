package herrontestprojectpackage;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class fontcolortestcalss {

	//Equipment for PDM view
	String plantName = "NBI lab", bldgName = "NBI test bench", equipName = "Oct15Test01";
	
	@BeforeTest
	public void initialization() throws Exception {
	
		webutils.launchFFBrowser();
		webutils.navigatetoURL();
		webutils.driver.findElement(By.xpath("//div[@class='navigation-bar']//a")).click();
	}
	
	@Test(priority=0)
	public void logintest() throws Exception {
		
		System.out.println("Starting of Login page.......");
		
		fontcolorclass.runningAllTags();
			
		System.out.println("Ending of Login page.......");
		System.out.println();
		webutils.driver.findElement(By.xpath("//div[@class='navigation-bar']//a")).click();
	}

	@Test(priority=1)
	public void dashboardtest() throws Exception {
		
		System.out.println("Starting of Dashboard page.......");	
		
		webutils.signinAs("admin", "pass@123");
		Assert.assertEquals("Dashboard", webutils.driver.getTitle());

		herrontestclass.dashboardView(plantName);
		fontcolorclass.runningAllTags();
		
		System.out.println("Ending of Dashboard page.......");
		System.out.println();
	}

	@Test(priority=2)
	public void addusertest() throws Exception {
		
		System.out.println("Starting of AddUser page.......");	
		
		herrontestclass.gotoAddUser();
		fontcolorclass.tagInput("button");
		
		herrontestclass.clickonAddUser();
		
		fontcolorclass.runningAllTags();
		fontcolorclass.tagInput("button");
		
		System.out.println("Ending of AddUser page.......");
		System.out.println();
	}

	@Test(priority=3)
	public void equipmentsetuptest() throws Exception {
		
		System.out.println("Starting of Equipment setup page.......");
		
		herrontestclass.gotoEquipmentSetup();

		fontcolorclass.runningAllTags();
		
		System.out.println("Ending of Equipment setup page.......");
		System.out.println();
	}

	@Test(priority=4)
	public void addequipmenttest() throws Exception {
		
	  	System.out.println("Starting of Add Equipment page.......");
	  	
		herrontestclass.gotoEquipmentSetup();
	  	herrontestclass.clickonAddEquipment();

		fontcolorclass.runningAllTags();
		fontcolorclass.tagInput("button");
		
		System.out.println("Ending of Add Equipment page.......");
		System.out.println();
	}

	@Test(priority=5)
	public void machinefaultstest() throws Exception {
				
		System.out.println("Starting of Machine Faults page.......");
	
		herrontestclass.gotoMachineFaults();
		fontcolorclass.tagInput("button");
		
		herrontestclass.clickonMachineFaults();
	
		fontcolorclass.runningAllTags();
		fontcolorclass.tagInput("button");
		
		herrontestclass.adddiagnosisCancel();
		
		System.out.println("Ending of Machine Faults page.......");
		System.out.println();
	}
	
	@Test(priority=6)
	public void sensortemplatetest() throws Exception {
				
		System.out.println("Starting of Sensor Template page.......");
	
		herrontestclass.gotoSensorTemplate();
		fontcolorclass.tagInput("button");
		
		herrontestclass.clickonAddSensorTemplate();
	
		fontcolorclass.runningAllTags();
		fontcolorclass.tagInput("button");
		
		herrontestclass.addsensortemplateCancel();
		
		System.out.println("Ending of Sensor Template page.......");
		System.out.println();
	}
	
	@Test(priority=7)
	public void bearingtemplatetest() throws Exception {
				
		System.out.println("Starting of Bearing Template page.......");
	
		herrontestclass.gotoBearingTemplate();
		fontcolorclass.tagInput("button");
		
		herrontestclass.clickonAddBearingTemplate();
	
		fontcolorclass.runningAllTags();
		fontcolorclass.tagInput("button");
		
		herrontestclass.addbearingtemplateCancel();
		
		System.out.println("Ending of Bearing Template page.......");
		System.out.println();
	}
	
	@Test(priority=8)
	public void motortemplatetest() throws Exception {
			
		System.out.println("Starting of Motor Template page.......");
			
		herrontestclass.gotoMotorTemplate();
		fontcolorclass.tagInput("button");
		
		herrontestclass.clickonAddMotorTemplate();

		fontcolorclass.runningAllTags();
		fontcolorclass.tagInput("button");
		
		herrontestclass.addmotortemplateCancel();
		
		System.out.println("Ending of Motor Template page.......");
		System.out.println();
	}
	
	@Test(priority=9)
	public void equipmenttypetemplatetest() throws Exception {
				
		System.out.println("Starting of Equipment Type Template page.......");
		
		herrontestclass.gotoEquipmentTypeTemplate();
		fontcolorclass.tagInput("button");
		
		herrontestclass.clickonAddEquipmentTypeTemplate();

		fontcolorclass.runningAllTags();
		fontcolorclass.tagInput("button");
		
		herrontestclass.addequipmenttypetemplateCancel();
		
		System.out.println("Ending of Equipment Type Template page.......");
		System.out.println();
	}
	
	@Test(priority=10)
	public void equipmenttemplatetest() throws Exception {
			
		System.out.println("Starting of Equipment Template page.......");
			
		herrontestclass.gotoEquipmentTemplate();
		fontcolorclass.tagInput("button");
		
		herrontestclass.clickonAddEquipmentTemplate();

		fontcolorclass.runningAllTags();
		fontcolorclass.tagInput("button");
		
		herrontestclass.addequipmenttemplateCancel();
		
		System.out.println("Ending of Equipment Template page.......");
		System.out.println();
	}
	
	@Test(priority=11)
	public void eventstest() throws Exception {
			
		System.out.println("Starting of Events page.......");	
			
		herrontestclass.gotoCreateNewPrivateFilter();
		Assert.assertEquals("Events", webutils.driver.getTitle());

		fontcolorclass.runningAllTags();
		
		System.out.println("Ending of Events page.......");
		System.out.println();
	}

	@Test(priority=12)
	public void pdmviewtest() throws Exception {
			
		System.out.println("Starting of PDM page.......");	
			
		herrontestclass.createNewFilter(plantName);
		herrontestclass.navigateResults(plantName, bldgName, equipName);
		herrontestclass.clickonaNeuron("0");		
		Assert.assertEquals("PDM View", webutils.driver.getTitle());

		fontcolorclass.runningAllTags();
		fontcolorclass.tagInput("button");
		
		System.out.println("Ending of PDM page.......");
		System.out.println();
	}

	@Test(priority=13)
	public void logout() throws Exception {
		
		webutils.logOut();
		Assert.assertEquals("Stafford Herron", webutils.driver.getTitle());
	}
	
	@AfterTest
	public void shutdown() throws Exception {
		
		webutils.terminate();
	}
}
