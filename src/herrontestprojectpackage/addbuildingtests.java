package herrontestprojectpackage;

import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

public class addbuildingtests {

	String plant = "Test1", building = "Building1";
	WebElement ele;
	
	@Test(priority=0)
	public void initialstep() throws Exception {
		
		webutils.launchFFBrowser();

		webutils.navigatetoURL();
		
		webutils.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		webutils.driver.manage().timeouts().setScriptTimeout(120, TimeUnit.SECONDS);
		
		webutils.signinAs("admin", "pass@123");
		Assert.assertEquals("Dashboard", webutils.driver.getTitle());
	}

	@Test(priority=1)
	public void addbuilding() throws Exception {
		
		herrontestclass.addBuilding(plant, building);
		
		webutils.driver.navigate().refresh();
		
		Assert.assertEquals(webutils.driver.getPageSource().contains(building), true);
	}
	
	@Test(priority=2)
	public void addbuildingDupBuilding() throws Exception {

		herrontestclass.addBuilding(plant, building);
		
		ele = webutils.driver.findElement(By.xpath("//*[@id='messages_container']/div/div/div/span"));
		Assert.assertEquals("Duplicate Building. Please use different name.", ele.getText());
		//Assert.assertEquals(true, webutils.driver.getPageSource().contains("Duplicate Building. Please use different name."));
	}

	@Test(priority=3)
	public void addbuildingNoPlant() throws Exception {

		//herrontestclass.addbuildingClear();
		herrontestclass.addbuildingCancel();
		
		herrontestclass.addBuilding("Select Plant", building);
		
		ele = webutils.driver.findElement(By.xpath("//*[@id='messages_container']/div/div/div/span"));
		Assert.assertEquals("Please select Plant.", ele.getText());
		//Assert.assertEquals(true, webutils.driver.getPageSource().contains("Please select Plant."));
	}
	
	@Test(priority=4)
	public void addbuildingNoBuilding() throws Exception {

		//herrontestclass.addbuildingClear();
		herrontestclass.addbuildingCancel();
		
		herrontestclass.addBuilding(plant, "");
		
		ele = webutils.driver.findElement(By.xpath("//*[@id='messages_container']/div/div/div/span"));
		Assert.assertEquals("Please Enter Building Name.", ele.getText());
		//Assert.assertEquals(true, webutils.driver.getPageSource().contains("Please Enter Building Name."));
	}
	
	@Test(priority=5)
	public void addbuildingNoPlantBuilding() throws Exception {

		//herrontestclass.addbuildingClear();
		herrontestclass.addbuildingCancel();
		
		herrontestclass.addBuilding("Select Plant", "");
		
		ele = webutils.driver.findElement(By.xpath("//*[@id='messages_container']/div/div/div/span"));
		Assert.assertEquals("Please Enter Building Name.", ele.getText());
		//Assert.assertEquals(true, webutils.driver.getPageSource().contains("Please Enter Building Name."));
	}

	@AfterTest
	public void shutdown() throws Exception {
		
		webutils.terminate();
	}
}

