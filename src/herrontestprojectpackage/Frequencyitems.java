package herrontestprojectpackage;

import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

public class Frequencyitems {

	//Set the data for Plant, Building, Equipment
	String plant = "Test16", building = "Building16", equipname = "Equip16", equipid = "7016"; 
	//.............................................................................................
	// Set the data for Add Bearing Template
	String make="SKF", model="6215", speedinput="Fixed - RPM", activeposition="1";
	//...........................................................................................
	//Set the data for Add Gearbox Templates
	String name="gearbox1", inputspeed="Fixed - RPM", drivesideteeth1="dkdk", drivesideteeth2="dkdk", gearactposition="1";
	
	
	//Screen Shots location
	String filepath="C:\\HerronTestProject";
	
	WebElement ele;
	
	@Test(priority=0)
	public void initialstep() throws Exception {
		
		//webutils.launchFFBrowser();
		webutils.launchChromeBrowser();
		
		webutils.navigatetoURL();
		
		webutils.driver.manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);
		webutils.driver.manage().timeouts().setScriptTimeout(240, TimeUnit.SECONDS);
		
		webutils.signinAs("planner", "pass@123");
		Assert.assertEquals("Stafford Herron", webutils.driver.getTitle());
	}

	@Test(priority=1)
	public void addplant() throws Exception {
		
		herrontestclass.addPlant(plant);
		
		//Verification		
		ele=webutils.driver.findElement(By.xpath("//*[@id='successMsgs']/div"));
		Assert.assertEquals("Plant added Successfully.", ele.getText());
						
			
	}
	
	@Test(priority=2)
	public void addbuilding() throws Exception {
		
		herrontestclass.addBuilding(plant, building);
		//Verification
		ele=webutils.driver.findElement(By.xpath("//*[@id='successMsgs']/div"));
		Assert.assertEquals("Building added Successfully.", ele.getText());
		
	}

	@Test(priority=3)
	public void addequipment() throws Exception {
		
		herrontestclass.addEquipment(plant, building, equipname, equipid);
		
		//Verification		
		ele=webutils.driver.findElement(By.xpath("//*[@id='successMsgs']/div"));
		Assert.assertEquals("Equipment Added Successfully.", ele.getText());
		
				
	}
	@Test(priority=4)
	public void frequency() throws Exception
	{
		herrontestclass.frequencybutton();
		herrontestclass.selectfrequency("Bearing");
	}
	
	@Test(priority=5)
	public void addBearing() throws Exception
	{
		herrontestclass.addBearing(make, model, speedinput, activeposition);
		
		//Verification		
		ele=webutils.driver.findElement(By.xpath("//*[@id='successMsgs']/div"));
		Assert.assertEquals("Bearing Frequency Item added Successfully", ele.getText());
		
		//scroll down 
		herrontestclass.scrolldown();
		
		// ScreenShot of Bearing table
				 webutils.takeaScreenShot(filepath+"\\bearingfrequencyitem.jpg");
		
	}
	@Test(priority=6)
	public void gearbox() throws Exception
	{
		herrontestclass.selectfrequency("Gearbox");
		herrontestclass.addgearboxTemplate(name, inputspeed, drivesideteeth1, drivesideteeth2, gearactposition);
		
		//Verification		
				ele=webutils.driver.findElement(By.xpath("//*[@id='successMsgs']/div"));
				Assert.assertEquals("Gear Box Frequency Item Added Successfully", ele.getText());
				
				//scroll down 
				herrontestclass.scrolldown();
				
				// ScreenShot of Bearing table
						 webutils.takeaScreenShot(filepath+"\\gearboxfrequencyitem.jpg");
				
		
		
		
	}
	
	
	
		
	@Test(priority=6)
	public void logout() throws Exception {
		
		//webutils.logOut();
		//Verification
		//Assert.assertEquals("Stafford Herron", webutils.driver.getTitle());
	}
	
	@Test(priority=7)
	public void close() throws Exception
	{
		// close the browser
		//webutils.driver.quit();
		
	}
	
	
	
	
	
	
	
		
}
