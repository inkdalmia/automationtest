package herrontestprojectpackage;


import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;


public class herrontestclass {
	
	private static WebDriverWait myWaitVar;
	
	//Hover to Configuration tab and select Equipment Setup
	public static void gotoEquipmentSetup() {
	
		WebElement configuration = webutils.driver.findElement(By.linkText("Configure"));

		Actions builder = new Actions(webutils.driver);
		Action mouseOverConfig = builder.moveToElement(configuration).build();
		mouseOverConfig.perform();
		
		webutils.driver.findElement(By.linkText("Equipment Setup")).click();
	}
	
	//Hover to Configuration tab and select Machine Faults and then click on Add Diagnosis
	public static void gotoMachineFaults() {
	
		WebElement configuration = webutils.driver.findElement(By.linkText("Configure"));

		Actions builder = new Actions(webutils.driver);
		Action mouseOverConfig = builder.moveToElement(configuration).build();
		mouseOverConfig.perform();
		
		webutils.driver.findElement(By.linkText("Machine Faults")).click();
	}
	
	//Click on MachineFaults
	public static void clickonMachineFaults() {
		
		webutils.driver.findElement(By.xpath("//form[@id='usermgmgtFrm']//span[text()='Add Diagnosis']")).click();
	}
	
	//Click on the cancel button on the Add Diagnosis
	public static void adddiagnosisCancel() {
		
		webutils.driver.findElement(By.xpath("//form[@id='diagnosis']//span[text()='Cancel']")).click();
	}
	
	//View Dashboard
	public static void dashboardView(String plantName) {
		
		webutils.driver.findElement(By.xpath("//form[@id='headerFrm']//label[.='Plants']")).click();
		webutils.driver.findElement(By.xpath("//div[@id='plants_panel']//li[label='"+plantName+"']")).click();
		
		webutils.driver.findElement(By.xpath("//button[@id='applyFilter']//span[text()='Apply']")).click();
	}
	
	//Click on Add Plant
	public static void clickonAddPlant() {
		
		webutils.driver.findElement(By.xpath("//div[@id='equipmentSetup']//span[text()='Add Plant']")).click();
	}
	
	// Add a plant
	public static void addPlant(String plantName) {
	
		gotoEquipmentSetup();
		clickonAddPlant();
	
		myWaitVar = new WebDriverWait(webutils.driver, 15);
		myWaitVar.until(ExpectedConditions.elementToBeClickable(By.id("plantId")));
		//webutils.driver.switchTo().window("addPlantId");
		focusthelatestwindow();
		
		
		//Enter the plant name
		webutils.driver.findElement(By.id("plantId")).sendKeys(plantName);
		
		//Click on Save button
		webutils.driver.findElement(By.xpath("//form[@id='plantAddFrm']//span[text()='Save']")).click();
	}
	
	//Click on the cancel button on the Add Plant window
	public static void addplantCancel() {
		
		webutils.driver.findElement(By.xpath("//form[@id='plantAddFrm']//span[text()='Cancel']")).click();
	}
	
	//Clear the text fields on the Add Plant window
	public static void addplantClear() {
		
		webutils.driver.findElement(By.id("plantId")).clear();
	}
	
	//Click on Add Building
	public static void clickonAddBuilding() {
		
		webutils.driver.findElement(By.xpath("//div[@id='equipmentSetup']//span[text()='Add Building']")).click();
	}
	
	//focus the latest window popup
	public static String latestwindowid;
	public static void focusthelatestwindow()
	{
		//String latestwindowid;
		Set<String> s=webutils.driver.getWindowHandles();
		Iterator<String> i=s.iterator();
		while(i.hasNext())
		{
			latestwindowid=i.next();
		}
		webutils.driver.switchTo().window(latestwindowid);
	}

	//Add a Building
	public static void addBuilding(String plantName, String bldgName) throws InterruptedException {
		
		gotoEquipmentSetup();
		clickonAddBuilding();
		focusthelatestwindow();
		
		WebDriverWait myWaitVar = new WebDriverWait(webutils.driver, 30);
		myWaitVar.until(ExpectedConditions.elementToBeClickable(By.id("selectedPlantId_label")));
	
		
		
		//webutils.driver.switchTo().frame("buildingAddFrm");
		//Select the existing plant
		webutils.driver.findElement(By.xpath("//*[@id='addBuildId_title']")).click();
           Thread.sleep(2000);
		//webutils.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		//webutils.driver.findElement(By.xpath("//*[@id='addBuildId']/div[2]")).click();
		//Using Class name
		//WebElement drop1=webutils.driver.findElement(By.className("ui-selectonemenu-label ui-inputfield ui-corner-all"));
		//drop1.click();
		
		
		//.//*[@id='selectedPlantId']/div[3]/span
		//using id
		//webutils.driver.findElement(By.id("selectedPlantId_label")).click();
		//Using Xpath
		webutils.driver.findElement(By.xpath("//*[@id='selectedPlantId_label']")).click();
		//Manual wrote  xpath of dropdown
		//webutils.driver.findElement(By.xpath("//input[@id='selectedPlantId_focus']//label[@id='selectedPlantId_label']")).click();
		//using anther id of dropdown
		//webutils.driver.findElement(By.id("selectedPlantId")).click();
		//webutils.driver.findElement(By.id("selectedPlantId")).click();
		
	//	webutils.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		
		//webutils.driver.findElement(By.className("ui-selectonemenu-label ui-inputfield ui-corner-all")).click();
		//webutils.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		
		// using CSS Slecter
		//webutils.driver.findElement(By.cssSelector("#selectedPlantId_label")).click();
		//webutils.driver.findElement(By.xpath("//*[@id='selectedPlantId']/div[3]/span")).click();
		//webutils.driver.findElement(By.xpath("//*[@id='selectedPlantId_label']")).click();
		webutils.driver.findElement(By.xpath("//div[@id='selectedPlantId_panel']//li[@data-label='"+plantName+"']")).click();
		
		
		//try{
	//	WebElement selectplant=webutils.driver.findElement(By.xpath("//div[@id='selectedPlantId_panel']//li[@data-label='"+plantName+"']"));
		//selectplant.click();
		//}
		//catch(StaleElementReferenceException s2)
		//{
		//	s2.toString();
		//}
		
		//Thread.sleep(5000);
		//webutils.driver.findElement(By.xpath("//*[@id='selectedPlantId_label']")).click();
		//webutils.driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		//webutils.driver.findElement(By.xpath("//*[@id='selectedPlantId_panel']/div")).click();
				
		//webutils.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		
		//WebElement existingplant=webutils.driver.findElement(By.xpath("//*[@id='selectedPlantId_label']"));
		//existingplant.click();
		//WebElement selectplant=webutils.driver.findElement(By.xpath("//div[@id='j_idt437_panel']//li[@data-label='"+plantName+"']"));
		//selectplant.click();
		

		//Enter the building name
		webutils.driver.findElement(By.id("buildingId")).sendKeys(bldgName);
		
		
		//Click on Save button
		webutils.driver.findElement(By.xpath("//form[@id='buildingAddFrm']//span[text()='Save']")).click();
		webutils.driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}
	
	//Click on the cancel button on the Add Building window
	public static void addbuildingCancel() {
		
		webutils.driver.findElement(By.xpath("//form[@id='buildingAddFrm']//span[text()='Cancel']")).click();
	}
	
	//Clear the text fields on the Add Building window
	public static void addbuildingClear() {
		
		webutils.driver.findElement(By.id("selectedPlantId_label")).clear();
		webutils.driver.findElement(By.id("buildingId")).clear();
	}
		
	//Click on Add Equipment
	public static void clickonAddEquipment() {
		
		webutils.driver.findElement(By.xpath("//div[@id='equipmentSetup']//span[text()='Add Equipment']")).click();
	}
	
	//Add an Equipment
	public static void addEquipment(String plantName, String bldgName, String equipName, String equipId) throws InterruptedException  {
		
		gotoEquipmentSetup();
		clickonAddEquipment();
		
		myWaitVar = new WebDriverWait(webutils.driver, 30);
		myWaitVar.until(ExpectedConditions.elementToBeClickable(By.xpath("//form[@id='plantBldFrm']//label[.='Select Plant']")));
		
		//webutils.driver.findElement(By.id("addItemLable")).click();
		
		webutils.driver.findElement(By.xpath("//*[@id='equipmentPage']")).click();
		Thread.sleep(2000);
		
		//Select the existing plant
		//webutils.driver.findElement(By.xpath("//*[@id='j_idt437_label']")).click();
		//webutils.driver.findElement(By.xpath("//*[@id='j_idt444_label']")).click();
		//webutils.driver.findElement(By.xpath("//div[@id='j_idt444_panel']//li[@data-label='"+plantName+"']")).click();
		//WebElement clickplantvalue=webutils.driver.findElement(By.xpath("//*[@id='j_idt444_label']"));
		///clickplantvalue.click();
		//WebElement selectvalue1=webutils.driver.findElement(By.xpath("//div[@id='j_idt444_panel']//li[@data-label='"+plantName+"']"));
		//selectvalue1.click();
		WebElement clickplantvalue=webutils.driver.findElement(By.xpath("//*[@id='plantsID']"));
		clickplantvalue.click();
		WebElement selectvalue1=webutils.driver.findElement(By.xpath("//div[@id='plantsID_panel']//li[@data-label='"+plantName+"']"));
		selectvalue1.click();
		Thread.sleep(2000);
		
		
		//webutils.driver.findElement(By.xpath("//div[@id='j_idt437_panel']//li[@data-label='"+plantName+"']")).click();
		
		
		Thread.sleep(4);
		myWaitVar =new WebDriverWait(webutils.driver, 20);
		myWaitVar.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='buildings_label']")));
		
		
		try
		{
			WebElement clickvalue1=webutils.driver.findElement(By.xpath("//*[@id='buildings_label']"));
			clickvalue1.click();
			WebElement selectvalue2=webutils.driver.findElement(By.xpath("//div[@id='buildings_panel']//li[@data-label='"+bldgName+"']"));
			selectvalue2.click();
			
			
		} catch(StaleElementReferenceException e2)
		{
			e2.toString();
		}
		
				
		//webutils.driver.manage().timeouts().implicitlyWait(16, TimeUnit.SECONDS)	;
		//Select the existing building
		//webutils.driver.findElement(By.id("buildings_label")).click();
		//webutils.driver.findElement(By.xpath("//*[@id='buildings']")).click();
		
		//webutils.driver.findElement(By.cssSelector("label#buildings_label")).click();
		//webutils.driver.findElement(By.xpath("//div[@id='buildings_panel']//li[@data-label='"+bldgName+"']")).click();
		
		//webutils.driver.findElement(By.xpath("//li[@data-label='"+bldgName+"']")).click();
		
		
				
		//Enter the equipment name
		webutils.driver.findElement(By.xpath("//*[@id='eqName']")).sendKeys(equipName);
	
		//Enter the equipment id
		webutils.driver.findElement(By.xpath("//*[@id='eqId']")).sendKeys(equipId);

		//Select the equipment type
		//webutils.driver.findElement(By.xpath("//form[@id='eqDetailsFrm']//label[.='Select Equipment Type']")).click();
		webutils.driver.findElement(By.xpath("//*[@id='eqType']")).click();
		
		
		webutils.driver.findElement(By.xpath("//*[@id='eqType_panel']/div/ul/li[2]")).click();
		
		myWaitVar =new WebDriverWait(webutils.driver, 15);
		myWaitVar.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='equipmentTemplates_label']")));
		Thread.sleep(3000);
	
		//Select the equipment template
		
		webutils.driver.findElement(By.id("equipmentTemplates_label")).click();
		//webutils.driver.findElement(By.xpath("//*[@id='equipmentTemplates_label']")).click();
		//webutils.driver.findElement(By.xpath("//*[@id='equipmentTemplates']/div[3]/span")).click();
		webutils.driver.findElement(By.xpath("//*[@id='equipmentTemplates_panel']/div/ul/li[4]")).click();
		
 		//Enter the fixed speed
		webutils.driver.findElement(By.id("sourceRPM")).sendKeys("2100");
		
		//Click on Save Equipment button
		//webutils.driver.findElement(By.id("saveEquipment")).click();
		webutils.driver.findElement(By.xpath("//*[@id='saveEquipment']")).click();
				
	}
	
	//Click on the cancel button on the Add Equipment window
	public static void addequipmentCancel() {
		
		webutils.driver.findElement(By.xpath("//form[@id='eqDetailsFrm']//span[text()='Cancel']")).click();
	}
	
	//Click on Add Monitor
	public static void clickonAddMonitor() {
		
		webutils.driver.findElement(By.xpath("//div[@id='equipmentSetup']//span[text()='Add Monitor']")).click();
	}
	
	//Click on 'Monitor and Sensor' tab
	public static void monitorandsensor()
	{
		webutils.driver.findElement(By.xpath("//*[@id='hierarchyTabView']/ul/li[4]")).click();
		// Click on Add Monitor button
		
		webutils.driver.findElement(By.xpath("//*[@id='hierarchyTabView:addMonitorSensor']")).click();
		webutils.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	}
	
	//Add a Monitor
	public static void addMonitor(String monitor, String serialno, String macadd, String connectivity, String ipoption) throws InterruptedException {
			
		WebDriverWait myWaitVar = new WebDriverWait(webutils.driver, 20);
		myWaitVar.until(ExpectedConditions.elementToBeClickable(By.id("barbeauName")));
		
		//Enter the Monitor name
		webutils.driver.findElement(By.id("barbeauName")).sendKeys(monitor);
		
		//Enter the Serial No
		webutils.driver.findElement(By.id("serialNo")).sendKeys(serialno);
		
		//Enter the MAC address
		webutils.driver.findElement(By.id("macAdd")).sendKeys(macadd);
		
		switch (connectivity) {
		case "Eth":
			//Select the Ethernet radio button
			webutils.driver.findElement(By.xpath("//*[@id='options1']/tbody/tr/td[1]/div/div[2]/span")).click();
			break;
		case "WL":
			//Select the Wireless radio button
			webutils.driver.findElement(By.xpath("//*[@id='options1']/tbody/tr/td[3]/div/div[2]/span")).click();
			
			webutils.driver.findElement(By.id("ssid")).sendKeys("123456");
			webutils.driver.findElement(By.xpath("//div[@id='securityType']//li[@data-label='WPA-PSK']")).click();
			webutils.driver.findElement(By.id("passKey")).sendKeys("123456");
			break;
		case "3G":
			//Select the 3G radio button
			webutils.driver.findElement(By.xpath("//*[@id='options1']/tbody/tr/td[5]/div/div[2]")).click();
			
			webutils.driver.findElement(By.xpath("//div[@id='tgServiceProvider']//label[.='T-Moblie']")).click();
			webutils.driver.findElement(By.xpath("//div[@id='j_idt209_panel']//li[@data-label='AT&T']")).click();
			break;
	    default:
	        break;
		}
		
		switch (ipoption) {
		case "Static":
			//Select the Static radio button
			webutils.driver.findElement(By.xpath("//*[@id='ipType']/tbody/tr/td[1]/div/div[2]/span")).click();
			
			webutils.driver.findElement(By.id("staticIpAdd")).sendKeys("111.111.111.111");
			webutils.driver.findElement(By.id("subnetMask")).sendKeys("112.112.112.112");
			webutils.driver.findElement(By.id("gateway")).sendKeys("113.113.113.113");
			break;
		case "DHCP":
			//Select the DHCP radio button
			webutils.driver.findElement(By.id("ipType:1")).click();
			break;
	    default:
	        break;
		}
			
		//Click on Save&Export button
		//webutils.driver.findElement(By.xpath("//form[@id='buttonsFrm']//span[text()='Save & Export']")).click();
		webutils.driver.findElement(By.xpath("//*[@id='saveExportId1']")).click();
		Thread.sleep(3000);
	}
	
	//Click on the cancel button on the Add Monitor window
	public static void addmonitorCancel() {
		
		//webutils.driver.findElement(By.xpath("//form[@id='barbeauFrm']//span[text()='Cancel']")).click();
		webutils.driver.findElement(By.xpath("//*[@id='j_idt199']")).click();
		
		
	}
	
	//Go to Plant Navigation, navigate to the equipment
	public static void plantNavigation(String plantName, String bldgName, String equipName) {
		
		webutils.driver.findElement(By.xpath("//form[@id='pdmConfigTreeFrm']//span[text()='" +plantName+ "']/ancestor::li//span[text()='" +bldgName+ "']/ancestor::li//span[text()='" +equipName+ "']")).click();
		//form[@id='pdmConfigTreeFrm']//span[text()='Test Plant']/ancestor::li//span[text()='Test Building']/ancestor::li//span[text()='Test Equip2']
	}
	
	//Select TestTemplate 
	public static void testTemplate(String tempname, String srn1) throws InterruptedException
	{
		// click on TestTemplate dropdown
		
		WebDriverWait myWaitVar = new WebDriverWait(webutils.driver, 30);
		myWaitVar.until(ExpectedConditions.elementToBeClickable(By.id("hierarchyTabView:monitorChannelDatatable:0:sensorTemplateId_label")));
		Thread.sleep(3000);
		
		//webutils.driver.findElement(By.xpath("//*[@id='hierarchyTabView:monitorsensorFrm']/div[2]")).click();
		webutils.driver.findElement(By.xpath("//*[@id='hierarchyTabView:monitorChannelDatatable:0:sensorTemplateId_label']")).click();
		webutils.driver.findElement(By.xpath("//*[@id='hierarchyTabView:monitorChannelDatatable:0:sensorTemplateId_panel']/div")).click();
		//webutils.driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		//Select the tempName
		webutils.driver.findElement(By.xpath("//div[@id='hierarchyTabView:monitorChannelDatatable:0:sensorTemplateId_panel']//div//ul//li[@data-label='"+tempname+"']")).click();
		
		webutils.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		// Enter Angle value
		
		//webutils.driver.findElement(By.id("hierarchyTabView:monitorChannelDatatable:0:angleTxt")).clear();		
	   // webutils.driver.findElement(By.id("hierarchyTabView:monitorChannelDatatable:0:angleTxt")).sendKeys("0");
		
		try
		{
			//Enter Angle Value
		webutils.driver.findElement(By.id("hierarchyTabView:monitorChannelDatatable:0:angleTxt")).clear();		
	    webutils.driver.findElement(By.id("hierarchyTabView:monitorChannelDatatable:0:angleTxt")).sendKeys("0");
				
			
		webutils.driver.findElement(By.xpath("//*[@id='hierarchyTabView:monitorChannelDatatable_data']/tr/td[4]")).click();
		//webutils.driver.findElement(By.id("hierarchyTabView:monitorChannelDatatable:0:calibrationId")).clear();
	    webutils.driver.findElement(By.id("hierarchyTabView:monitorChannelDatatable:0:calibrationId")).sendKeys("100");
	    //webutils.driver.findElement(By.xpath("//*[@id='hierarchyTabView:monitorChannelDatatable:0:calibrationId']")).sendKeys("100");
		}
		catch(ElementNotVisibleException e2)
		{
			e2.toString();
		}
		// Enter Sensor Serial No:1
	    webutils.driver.findElement(By.id("hierarchyTabView:monitorChannelDatatable:0:serialNameId")).clear();
	   // webutils.driver.findElement(By.id("hierarchyTabView:monitorChannelDatatable:0:serialNameId")).sendKeys("1");
	    webutils.driver.findElement(By.id("hierarchyTabView:monitorChannelDatatable:0:serialNameId")).sendKeys(srn1);
	    //OV Warning Limit
	    webutils.driver.findElement(By.id("hierarchyTabView:monitorChannelDatatable:0:ovwarningLimitId")).clear();
	    webutils.driver.findElement(By.id("hierarchyTabView:monitorChannelDatatable:0:ovwarningLimitId")).sendKeys("0.01");
	    //OV Critical Limit
	    webutils.driver.findElement(By.id("hierarchyTabView:monitorChannelDatatable:0:ovcriticalLimitId")).clear();
	    webutils.driver.findElement(By.id("hierarchyTabView:monitorChannelDatatable:0:ovcriticalLimitId")).sendKeys("0.25");
	    //Posisition and Direction
	    webutils.driver.findElement(By.xpath("//div[@id='hierarchyTabView:monitorChannelDatatable:0:positionSl']/div[3]/span")).click();
	    webutils.driver.findElement(By.xpath("//div[@id='hierarchyTabView:monitorChannelDatatable:0:positionSl_panel']/div/ul/li[2]")).click();
	    webutils.driver.findElement(By.xpath("//div[@id='hierarchyTabView:monitorChannelDatatable:0:directionId']/div[3]/span")).click();
	    webutils.driver.findElement(By.xpath("//div[@id='hierarchyTabView:monitorChannelDatatable:0:directionId_panel']/div/ul/li[2]")).click();
	    //Channel
	    webutils.driver.findElement(By.id("hierarchyTabView:monitorChannelDatatable:0:channelId_label")).click();
	    webutils.driver.findElement(By.xpath("//div[@id='hierarchyTabView:monitorChannelDatatable:0:channelId_panel']/div/ul/li[2]")).click();
		
		
		//webutils.driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		//Select Angle
		//webutils.driver.findElement(By.id("hierarchyTabView:monitorChannelDatatable:0:j_idt791")).clear();
		//webutils.driver.findElement(By.id("hierarchyTabView:monitorChannelDatatable:0:j_idt791")).click();
		//webutils.driver.findElement(By.id("hierarchyTabView:monitorChannelDatatable:0:j_idt791")).sendKeys("0");
		//webutils.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		//Select Calibration
		// webutils.driver.findElement(By.id("hierarchyTabView:monitorChannelDatatable:0:calibrationId")).clear();
		//webutils.driver.findElement(By.id("hierarchyTabView:monitorChannelDatatable:0:calibrationId")).sendKeys("100");
		//webutils.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		//Select Serial No:1
		//webutils.driver.findElement(By.id("hierarchyTabView:monitorChannelDatatable:0:serialNameId")).clear();
		//webutils.driver.findElement(By.id("hierarchyTabView:monitorChannelDatatable:0:serialNameId")).sendKeys("1");
		//webutils.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		//Select OV Warning Limit:0.01
		//webutils.driver.findElement(By.id("hierarchyTabView:monitorChannelDatatable:0:ovwarningLimitId")).sendKeys("0.01");
		//webutils.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		//Select OV Critical Limit:0.25
		//webutils.driver.findElement(By.id("hierarchyTabView:monitorChannelDatatable:0:ovcriticalLimitId")).sendKeys("0.25");
		//webutils.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		//Click on Position/Direction 
		//Angle
		//((JavascriptExecutor) webutils.driver).executeScript("document.getElementById('hierarchyTabView:monitorChannelDatatable:0:j_idt791').value = '0';");
		
		//((JavascriptExecutor) webutils.driver).executeScript("document.getElementById('hierarchyTabView:monitorChannelDatatable:0:calibrationId').value = '100';");
		
		
		
		
		//webutils.driver.findElement(By.xpath("//*[@id='hierarchyTabView:monitorChannelDatatable:0:positionSl_label']")).click();
		//webutils.driver.findElement(By.xpath("//*[@id='hierarchyTabView:monitorChannelDatatable:0:positionSl_panel']/div/ul/li[2]")).click();
		//webutils.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		//webutils.driver.findElement(By.xpath("//*[@id='hierarchyTabView:monitorChannelDatatable:0:directionId_label']")).click();
		//webutils.driver.findElement(By.xpath("//*[@id='hierarchyTabView:monitorChannelDatatable:0:directionId_panel']/div/ul/li[2]")).click();
		//webutils.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		//Select Channel
		//webutils.driver.findElement(By.xpath("//*[@id='hierarchyTabView:monitorChannelDatatable:0:j_idt806_label']")).click();
		//webutils.driver.findElement(By.xpath("//*[@id='hierarchyTabView:monitorChannelDatatable:0:j_idt806_panel']/div/ul/li[2]")).click();
				
		
	}
	
	//click on Add new Sensor button
	public static void addnewsensor(String tempname, String srn2)
	{
		//click on 'Add new sensor' Button
		webutils.driver.findElement(By.xpath("//*[@id='hierarchyTabView:addSensor']")).click();
		//Enter details to Second row
		//Enter Name 
		webutils.driver.findElement(By.xpath("//*[@id='hierarchyTabView:monitorChannelDatatable:1:sensorNameId']")).sendKeys("S2");
		//Select the test template
		webutils.driver.findElement(By.xpath("//*[@id='hierarchyTabView:monitorChannelDatatable:1:sensorTemplateId_label']")).click();
		webutils.driver.findElement(By.xpath("//div[@id='hierarchyTabView:monitorChannelDatatable:1:sensorTemplateId_panel']//div//ul//li[@data-label='"+tempname+"']")).click();
		//Select Angle
		webutils.driver.findElement(By.id("hierarchyTabView:monitorChannelDatatable:1:angleTxt")).clear();
		webutils.driver.findElement(By.id("hierarchyTabView:monitorChannelDatatable:1:angleTxt")).sendKeys("0");
		//webutils.driver.findElement(By.xpath("//*[@id='hierarchyTabView:monitorChannelDatatable:1:j_idt791']")).sendKeys("0");
		//Select Calibration
		webutils.driver.findElement(By.xpath("//*[@id='hierarchyTabView:monitorChannelDatatable:1:calibrationId']")).sendKeys("100");
		//Select Serial No.2
		webutils.driver.findElement(By.xpath("//*[@id='hierarchyTabView:monitorChannelDatatable:1:serialNameId']")).sendKeys(srn2);
		//Select OV Warning Limit:0.01
		webutils.driver.findElement(By.xpath("//*[@id='hierarchyTabView:monitorChannelDatatable:1:ovwarningLimitId']")).sendKeys("0.01");
		//Select OV Critical Limit:0.25
		webutils.driver.findElement(By.xpath("//*[@id='hierarchyTabView:monitorChannelDatatable:1:ovcriticalLimitId']")).sendKeys("0.25");
		//Click on Position/Direction 
	    webutils.driver.findElement(By.xpath("//div[@id='hierarchyTabView:monitorChannelDatatable:1:positionSl']/div[3]/span")).click();
	    webutils.driver.findElement(By.xpath("//div[@id='hierarchyTabView:monitorChannelDatatable:1:positionSl_panel']/div/ul/li[3]")).click();
	    webutils.driver.findElement(By.xpath("//div[@id='hierarchyTabView:monitorChannelDatatable:1:directionId']/div[3]/span")).click();
	    webutils.driver.findElement(By.xpath("//div[@id='hierarchyTabView:monitorChannelDatatable:1:directionId_panel']/div/ul/li[3]")).click();
		//webutils.driver.findElement(By.xpath("//*[@id='hierarchyTabView:monitorChannelDatatable:1:positionSl_label']")).click();
		//webutils.driver.findElement(By.xpath("//*[@id='hierarchyTabView:monitorChannelDatatable:1:positionSl_panel']/div/ul/li[3]")).click();
		//Click on Direction
		//webutils.driver.findElement(By.xpath("//*[@id='hierarchyTabView:monitorChannelDatatable:1:directionId_label']")).click();
		//webutils.driver.findElement(By.xpath("//*[@id='hierarchyTabView:monitorChannelDatatable:1:directionId_panel']/div/ul/li[3]")).click();
		//Select Channel
		webutils.driver.findElement(By.id("hierarchyTabView:monitorChannelDatatable:1:channelId_label")).click();
	    webutils.driver.findElement(By.xpath("//div[@id='hierarchyTabView:monitorChannelDatatable:1:channelId_panel']/div/ul/li[3]")).click();
		//webutils.driver.findElement(By.xpath("//*[@id='hierarchyTabView:monitorChannelDatatable:1:j_idt805_label']")).click();
	  // webutils.driver.findElement(By.xpath("//*[@id='hierarchyTabView:monitorChannelDatatable:1:j_idt805_panel']/div/ul/li[3]")).click();
	   //Click on Save Button
	   webutils.driver.findElement(By.xpath("//*[@id='hierarchyTabView:save']")).click();
			
	}
	
	
	//Attach a Monitor
	public static void attachMonitor(String monitorName) {

		//Go to the Monitor & Sensor tab
		webutils.driver.findElement(By.xpath("//div[@id='hierarchyTabView']//.[text()='Monitor & Sensor']")).click();
		
		//Select the existing monitor
		webutils.driver.findElement(By.id("hierarchyTabView:monitorID_label")).click();
		webutils.driver.findElement(By.xpath("//div[@id='hierarchyTabView:monitorID_panel']//li[@data-label='"+monitorName+"']")).click();

		//Click on Attach button
		webutils.driver.findElement(By.xpath("//div[@class='monitor-option']//span[text()='Attach']")).click();
	}
	
	//Fill in the Sensor Map Table (Sprint 21 UI)
	public static void newUIsensorMapTable(int rownum, String sensornameid, String tempname, String angle, String calibration, String serialno, String ovwlimit, String ovclimit, String position, String direction, String channel) {
		
		webutils.driver.findElement(By.id("hierarchyTabView:monitorChannelDatatable:" +rownum+ ":sensorNameId")).sendKeys(sensornameid);
		
		webutils.driver.findElement(By.id("hierarchyTabView:monitorChannelDatatable:" +rownum+ ":sensorTemplateId_label")).click();
		webutils.driver.findElement(By.xpath("//div[@id='hierarchyTabView:monitorChannelDatatable:" +rownum+ ":sensorTemplateId_panel']//li[@data-label='" +tempname+ "']")).click();

		webutils.driver.findElement(By.id("hierarchyTabView:monitorChannelDatatable:" +rownum+ ":j_idt799")).sendKeys(angle);
		
		webutils.driver.findElement(By.id("hierarchyTabView:monitorChannelDatatable:" +rownum+ ":calibrationId")).sendKeys(calibration);

		webutils.driver.findElement(By.id("hierarchyTabView:monitorChannelDatatable:" +rownum+ ":serialNameId")).sendKeys(serialno);
		
		webutils.driver.findElement(By.id("hierarchyTabView:monitorChannelDatatable:" +rownum+ ":ovwarningLimitId")).sendKeys(ovwlimit);
		
		webutils.driver.findElement(By.id("hierarchyTabView:monitorChannelDatatable:" +rownum+ ":ovcriticalLimitId")).sendKeys(ovclimit);
		
		webutils.driver.findElement(By.id("hierarchyTabView:monitorChannelDatatable:" +rownum+ ":positionSl_label")).click();
		webutils.driver.findElement(By.xpath("//div[@id='hierarchyTabView:monitorChannelDatatable:" +rownum+ ":positionSl_panel']//li[@data-label='" +position+ "']")).click();
		
		webutils.driver.findElement(By.id("hierarchyTabView:monitorChannelDatatable:" +rownum+ ":directionId_label")).click();
		webutils.driver.findElement(By.xpath("//div[@id='hierarchyTabView:monitorChannelDatatable:" +rownum+ ":directionId_panel']//li[@data-label='" +direction+ "']")).click();

		webutils.driver.findElement(By.id("hierarchyTabView:monitorChannelDatatable:" +rownum+ ":j_idt814_label")).click();
		webutils.driver.findElement(By.xpath("//div[@id='hierarchyTabView:monitorChannelDatatable:" +rownum+ ":j_idt814_panel']//li[@data-label='" +channel+ "']")).click();
	}
	
	//Fill in the Sensor Map Table
	public static void sensorMapTable(String rownum, String position, String direction, String sensorname, String tempname, String calibration, String serialno, String ovwlimit, String ovclimit) {
		
		webutils.driver.findElement(By.id("hierarchyTabView:monitorChannelDatatable:" +rownum+ ":positionSl_label")).click();
		webutils.driver.findElement(By.xpath("//div[@id='hierarchyTabView:monitorChannelDatatable:" +rownum+ ":positionSl_panel']//li[@data-label='" +position+ "']")).click();
		
		webutils.driver.findElement(By.id("hierarchyTabView:monitorChannelDatatable:" +rownum+ ":directionId_label")).click();
		webutils.driver.findElement(By.xpath("//div[@id='hierarchyTabView:monitorChannelDatatable:" +rownum+ ":directionId_panel']//li[@data-label='" +direction+ "']")).click();

		webutils.driver.findElement(By.id("hierarchyTabView:monitorChannelDatatable:" +rownum+ ":sensorNameId")).sendKeys(sensorname);
		
		webutils.driver.findElement(By.id("hierarchyTabView:monitorChannelDatatable:" +rownum+ ":sensorTemplateId_label")).click();
		webutils.driver.findElement(By.xpath("//div[@id='hierarchyTabView:monitorChannelDatatable:" +rownum+ ":sensorTemplateId_panel']//li[@data-label='" +tempname+ "']")).click();

		webutils.driver.findElement(By.id("hierarchyTabView:monitorChannelDatatable:" +rownum+ ":calibrationId")).sendKeys(calibration);

		webutils.driver.findElement(By.id("hierarchyTabView:monitorChannelDatatable:" +rownum+ ":serialNameId")).sendKeys(serialno);
		
		webutils.driver.findElement(By.id("hierarchyTabView:monitorChannelDatatable:" +rownum+ ":ovwarningLimitId")).sendKeys(ovwlimit);
		
		webutils.driver.findElement(By.id("hierarchyTabView:monitorChannelDatatable:" +rownum+ ":ovcriticalLimitId")).sendKeys(ovclimit);
	}
	
	public static String sensormaptableStartLocation() {
		
		WebElement ele = webutils.driver.findElement(By.xpath("(//tbody[@id='hierarchyTabView:monitorChannelDatatable_data']//select)[1]/ancestor::tr"));
		String rownum = ele.getAttribute("data-ri");
		
		return rownum;
	}
	
	//Click on SensorMap Save button
	public static void sensormapSave() {
		
		webutils.driver.findElement(By.id("hierarchyTabView:save")).click();
	}
	
	//Go to Sensor Template
	public static void gotoSensorTemplate() {
		
		WebElement configuration = webutils.driver.findElement(By.linkText("Configure"));

		Actions builder = new Actions(webutils.driver);
		Action mouseOverConfig = builder.moveToElement(configuration).build();
		mouseOverConfig.perform();
		
		WebElement templates = webutils.driver.findElement(By.linkText("Templates"));
		
		builder = new Actions(webutils.driver);
		mouseOverConfig = builder.moveToElement(templates).build();
		mouseOverConfig.perform();
		
		webutils.driver.findElement(By.linkText("Sensor Templates")).click(); 
		
	}  
	
	
	
	//Click on Add Sensor Template
	public static void clickonAddSensorTemplate() {
		
		webutils.driver.findElement(By.xpath("//form[@id='SensorTemplateForm']//span[text()='Add Sensor Template']")).click();
		
		
	}
	
	// Scroll down application 
	public static void scrolldown()
	{
		((JavascriptExecutor) webutils.driver).executeScript("window.scrollBy(0,300)", "");
		
	}
	
	// Scroll down application of analyst
	public static void scrolldownofanalyst()
	{
		((JavascriptExecutor) webutils.driver).executeScript("window.scrollBy(0,500)", "");
		
	}
	
		
	//Select Monitor 'Existing Monitor(BBU01) from Monitor Dropdown
	public static void existingMonitor(String monitor) throws InterruptedException
	
	{
		//WebElement clickplantvalue=webutils.driver.findElement(By.xpath("//*[@id='j_idt437_label']"));
		//clickplantvalue.click();
		//WebElement selectvalue1=webutils.driver.findElement(By.xpath("//div[@id='j_idt437_panel']//li[@data-label='"+plantName+"']"));
		//selectvalue1.click();
		//WebDriverWait myWaitVar = new WebDriverWait(webutils.driver, 15);
		//myWaitVar.until(ExpectedConditions.elementToBeClickable(By.id("hierarchyTabView:monitorID_label")));
		//webutils.driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		Thread.sleep(3000);
		//Assert.assertEquals(true, webutils.driver.getPageSource().contains("Sensor Map "));
		//WebElement selectvalue1=webutils.driver.findElement(By.xpath("//div[@id='j_idt437_panel']//li[@data-label='"+plantName+"']"));
		
		//webutils.driver.findElement(By.id("hierarchyTabView:j_idt773")).click();
		
		//*[@id='hierarchyTabView:monitorsensor']
		webutils.driver.findElement(By.xpath("//*[@id='hierarchyTabView:monitorsensor']")).click();
		
		
		WebElement clickdropdown=webutils.driver.findElement(By.id("hierarchyTabView:monitorID_label"));
		clickdropdown.click();
		//List<WebElement> option=clickdropdown.findElements(By.tagName("li"));
		//WebElement data=webutils.driver.findElement(By.xpath("//*[@id='hierarchyTabView:monitorID_panel']/div/ul/li["+monitor+"]"));
		//data.click();
		//*[@id='hierarchyTabView:monitorID_panel']/div
		//webutils.driver.findElement(By.xpath("//*[@id='hierarchyTabView:monitorID_panel']/div")).click();
		//webutils.driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
		
		//hierarchyTabView:monitorID_panel
		try{
		webutils.driver.findElement(By.xpath("//div[@id='hierarchyTabView:monitorID_panel']//div//ul//li[@data-label='"+monitor+"']")).click();
		
		}
		catch(NoSuchElementException n1)
		{
			n1.toString();
		}
		//webutils.driver.findElement(By.xpath("//div[@id='hierarchyTabView:monitorID_label']//li[text()='"+monitor+"']")).click();
		
		
		
		
		//*[@id='hierarchyTabView:monitorID_panel']/div/ul/li[4]
		//webutils.driver.findElement(By.xpath("//*[@id='hierarchyTabView:monitorID_panel']/div/ul/li["+monitor+"]")).click();
		//webutils.driver.findElement(By.xpath("//*[@id='hierarchyTabView:monitorID_label']//li[@data-label='"+monitor+"']")).click();
		
								
		//webutils.driver.findElement(By.xpath("//*[@id='hierarchyTabView:monitorID_label']")).click();
		
		//webutils.driver.findElement(By.id("hierarchyTabView:monitorID_label")).click();
		//webutils.driver.findElement(By.xpath("//*[@id='hierarchyTabView:monitorID_label']//li[@data-label='"+monitor+"']")).click();
		//webutils.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		//webutils.driver.findElement(By.xpath("//form[@id='plantAddFrm']//span[text()='Save']")).click();
		
		 //firefox //*[@id='hierarchyTabView:j_idt778']
		WebDriverWait myWaitVar = new WebDriverWait(webutils.driver, 30);
		myWaitVar.until(ExpectedConditions.elementToBeClickable(By.id("hierarchyTabView:attachBtn")));
		//myWaitVar.until(ExpectedConditions.visibilityOf(By.id("hierarchyTabView:attachBtn")));
		Thread.sleep(2000);
		
					
		// click on Attach Button
		//webutils.driver.findElement(By.xpath("//*[@id='hierarchyTabView:attachBtn']")).click();
		
		try{
		 WebElement attach=webutils.driver.findElement(By.xpath("//form[@id='hierarchyTabView:monitorsensorFrm']//span[text()='Attach']"));
		 attach.click();
		}
		catch(ElementNotVisibleException n1)
		{
			n1.toString();
		}
		//webutils.driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		//Enter Name field
		webutils.driver.findElement(By.xpath("//*[@id='hierarchyTabView:monitorChannelDatatable:0:sensorNameId']")).sendKeys("S1");
		//Click on "+" sign displayed in Template Column
		
		//webutils.driver.findElement(By.xpath("//*[@id='hierarchyTabView:monitorChannelDatatable:0:j_idt787']")).click();
		
		webutils.driver.findElement(By.xpath("//*[@id='hierarchyTabView:monitorChannelDatatable:0:sensorCreation']")).click();
		
		webutils.driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}
	
	
	//Fill in the Sensor Template
	public static void sensorTemplate(String make, String model, String sensitivity, String bias) throws InterruptedException {
		
		Thread.sleep(3000);
		focusthelatestwindow();
				
		webutils.driver.findElement(By.xpath("//*[@id='sensorTemplateId_title']")).click();
		
		//webutils.driver.findElement(By.id("sensorTemplateform:templateIT")).sendKeys(tempname);
		//webutils.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		webutils.driver.findElement(By.id("sensorTemplateform:makeId_input")).sendKeys(make);
		webutils.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		webutils.driver.findElement(By.id("sensorTemplateform:modelId_input")).sendKeys(model);
		webutils.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		webutils.driver.findElement(By.id("sensorTemplateform:nominalSensitivityIT")).sendKeys(sensitivity);
		webutils.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		//webutils.driver.findElement(By.id("sensorTemplateform:nativeUnitsIT")).sendKeys(units);
		webutils.driver.findElement(By.id("sensorTemplateform:baisVoltageIT")).sendKeys(bias);
		webutils.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		
		//Click on the save button on the Add Sensor Template
		//.//*[@id='j_idt923:j_idt925']
		//webutils.driver.findElement(By.xpath("//*[@id='j_idt923:j_idt925']")).click();
		//webutils.driver.findElement(By.xpath("//*[@id='saveTemplate']")).click();
		
		webutils.driver.findElement(By.xpath("//*[@id='saveButton']")).click();
		
		//webutils.driver.findElement(By.xpath("//form[@id='sensorTemplateform']//span[text()='Save ']")).click();
		webutils.driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		//webutils.driver.switchTo().window("Sensor Template");
		
		//webutils.driver.findElement(By.xpath("//*[@id='sensorTemplateId']/div[2]")).click();
	
		//webutils.driver.findElement(By.id("sensorTemplateform:templateIT")).sendKeys(tempname);
		//webutils.driver.findElement(By.id("sensorTemplateform:makeId_input")).sendKeys(make);
		//webutils.driver.findElement(By.id("sensorTemplateform:modelId_input")).sendKeys(model);
		//webutils.driver.findElement(By.id("sensorTemplateform:nominalSensitivityIT")).sendKeys(sensitivity);
		//webutils.driver.findElement(By.id("sensorTemplateform:nativeUnitsIT")).sendKeys(units);
		//webutils.driver.findElement(By.id("sensorTemplateform:baisVoltageIT")).sendKeys(bias);
		//webutils.driver.findElement(By.xpath("//*[@id='sensorTemplateform:templateIT']")).sendKeys(tempname);
		//webutils.driver.findElement(By.xpath("//*[@id='sensorTemplateform:makeId_input']")).sendKeys(make);
		//webutils.driver.findElement(By.xpath("//*[@id='sensorTemplateform:modelId_input']")).sendKeys(model);
		//webutils.driver.findElement(By.xpath("//*[@id='sensorTemplateform:nominalSensitivityIT']")).sendKeys(sensitivity);
		//webutils.driver.findElement(By.xpath("//*[@id='sensorTemplateform:baisVoltageIT']")).sendKeys(bias);
				
		
		//webutils.driver.findElement(By.xpath("//*[@id='j_idt922:j_idt924']")).click();
	}
	
	//Click on the cancel button on the Add Sensor Template
	public static void addsensortemplateCancel() {
		
		webutils.driver.findElement(By.xpath("//form[@id='sensorTemplateform']//span[text()='Cancel']")).click();
	}
	
	//Go to Bearing Template
	public static void gotoBearingTemplate() {
		
		WebElement configuration = webutils.driver.findElement(By.linkText("Configure"));

		Actions builder = new Actions(webutils.driver);
		Action mouseOverConfig = builder.moveToElement(configuration).build();
		mouseOverConfig.perform();
		
		WebElement templates = webutils.driver.findElement(By.linkText("Templates"));
		
		builder = new Actions(webutils.driver);
		mouseOverConfig = builder.moveToElement(templates).build();
		mouseOverConfig.perform();
		
		webutils.driver.findElement(By.linkText("Bearing Templates")).click();
	}
	
	//Click on Add Bearing Template
	public static void clickonAddBearingTemplate() {
		
		webutils.driver.findElement(By.xpath("//form[@id='BearingTemplateForm']//span[text()='Add Bearing Template']")).click();
	}
	
	//Click on cancel button on Add Bearing Template
	public static void addbearingtemplateCancel() {
		
		webutils.driver.findElement(By.xpath("//form[@id='bearingTemplateform']//span[text()='Cancel']")).click();
	}
	
	//Go to Motor Template
	public static void gotoMotorTemplate() {
		
		WebElement configuration = webutils.driver.findElement(By.linkText("Configure"));

		Actions builder = new Actions(webutils.driver);
		Action mouseOverConfig = builder.moveToElement(configuration).build();
		mouseOverConfig.perform();
		
		WebElement templates = webutils.driver.findElement(By.linkText("Templates"));
		
		builder = new Actions(webutils.driver);
		mouseOverConfig = builder.moveToElement(templates).build();
		mouseOverConfig.perform();
		
		webutils.driver.findElement(By.linkText("Motor Templates")).click();
	}
	
	//Click on Add Motor Template
	public static void clickonAddMotorTemplate() {
		
		webutils.driver.findElement(By.xpath("//form[@id='MotorTemplateForm']//span[text()='Add Motor Template']")).click();
	}
	
	//Click on cancel button on Add Motor Template
	public static void addmotortemplateCancel() {
		
		webutils.driver.findElement(By.xpath("//form[@id='motorTemplateform']//span[text()='Cancel']")).click();
	}
	
	//Go to Equipment Type Template
	public static void gotoEquipmentTypeTemplate() {
		
		WebElement configuration = webutils.driver.findElement(By.linkText("Configure"));

		Actions builder = new Actions(webutils.driver);
		Action mouseOverConfig = builder.moveToElement(configuration).build();
		mouseOverConfig.perform();
		
		WebElement templates = webutils.driver.findElement(By.linkText("Templates"));
		
		builder = new Actions(webutils.driver);
		mouseOverConfig = builder.moveToElement(templates).build();
		mouseOverConfig.perform();
		
		webutils.driver.findElement(By.linkText("Equipment Type")).click();
	}
	
	//Click on Add Equipment Type Template
	public static void clickonAddEquipmentTypeTemplate() {
		
		webutils.driver.findElement(By.xpath("//form[@id='equipmentTypeForm']//span[text()='Add Equipment Type']")).click();
	}
	
	//Click on cancel button on Add Equipment Type Template
	public static void addequipmenttypetemplateCancel() {
		
		webutils.driver.findElement(By.xpath("//form[@id='equipmentTypeAddEditform']//span[text()='Cancel']")).click();
	}
	
	//Go to Equipment Template
	public static void gotoEquipmentTemplate() {
		
		WebElement configuration = webutils.driver.findElement(By.linkText("Configure"));

		Actions builder = new Actions(webutils.driver);
		Action mouseOverConfig = builder.moveToElement(configuration).build();
		mouseOverConfig.perform();
		
		WebElement templates = webutils.driver.findElement(By.linkText("Templates"));
		
		builder = new Actions(webutils.driver);
		mouseOverConfig = builder.moveToElement(templates).build();
		mouseOverConfig.perform();
		
		webutils.driver.findElement(By.linkText("Equipment Template")).click();
	}
	
	//Click on Add Equipment Template
	public static void clickonAddEquipmentTemplate() {
		
		webutils.driver.findElement(By.xpath("//div[@id='form-content-2']//span[text()='Add Template']")).click();
	}
	
	//Click on cancel button on Add Equipment Template
	public static void addequipmenttemplateCancel() {
		
		webutils.driver.findElement(By.xpath("//form[@id='equipmentTemplateForm']//span[text()='Cancel']")).click();
	}
	
	//Go to create New Private Filter
	public static void gotoCreateNewPrivateFilter() {
		
		WebElement events = webutils.driver.findElement(By.linkText("Events"));

		Actions builder = new Actions(webutils.driver);
		Action mouseOverConfig = builder.moveToElement(events).build();
		mouseOverConfig.perform();
		
		WebElement privatefilter = webutils.driver.findElement(By.linkText("Private Filters"));
		
		builder = new Actions(webutils.driver);
		mouseOverConfig = builder.moveToElement(privatefilter).build();
		mouseOverConfig.perform();
		
		webutils.driver.findElement(By.linkText("Create New Filter")).click();
	}
	
	//Go to create New Public Filter
	public static void gotoCreateNewPublicFilter() {
		
		WebElement events = webutils.driver.findElement(By.linkText("Events"));

		Actions builder = new Actions(webutils.driver);
		Action mouseOverConfig = builder.moveToElement(events).build();
		mouseOverConfig.perform();
		
		WebElement publicfilter = webutils.driver.findElement(By.linkText("Public Filters"));
		
		builder = new Actions(webutils.driver);
		mouseOverConfig = builder.moveToElement(publicfilter).build();
		mouseOverConfig.perform();
		
		webutils.driver.findElement(By.linkText("Create New Filter")).click();
	}
	
	//Create New Private Filter
	public static void createNewFilter(String plantName) {
		
		webutils.driver.findElement(By.xpath("//div[@id='plants']//label[.='Plants']")).click();
		webutils.driver.findElement(By.xpath("//div[@id='plants_panel']//li[label='"+plantName+"']")).click();
		
		webutils.driver.findElement(By.xpath("//button[@id='applyFilter']//span[text()='Apply']")).click();
	}
	
	//Double click on Equipment
	public static void doubleclickonequipment() throws InterruptedException
	{
		//Double click on Equipment
		Actions act = new Actions(webutils.driver);
	    act.doubleClick(webutils.driver.findElement(By.xpath("//*[@id='pdmTree:0_0_0']/span/span[3]/span"))).perform();
	    Thread.sleep(500);
	    //click on any Sensor Name displayed in Name & Location column
	    //webutils.driver.findElement(By.xpath("//*[@id='alaramTabView:sensorAlarms:3:j_idt174']")).click();
	    webutils.driver.findElement(By.xpath("//*[@id='alaramTabView:sensorAlarms:0:j_idt226']")).click();
	    //webutils.driver.findElement(By.xpath("//a[Start-with(@id,'alaramTabView:sensorAlarms:0:j_idt_')]")).click();
	   // webutils.driver.findElement(By.className("ui-commandlink ui-widget")).click();
	    Thread.sleep(5000);
	    	    
	    	}
	
	public static void doubleclickovlimit() throws InterruptedException
	{
		//double click on any point of OV limit graph
		Actions act = new Actions(webutils.driver);
	    act.doubleClick(webutils.driver.findElement(By.xpath("//div[@class='ov-chart-min']//canvas"))).perform();
	    Thread.sleep(500);
	   // webutils.driver.findElement(By.xpath("//div[@id='sensorBottomTabView:ovDetailsPanel']//a/span")).click();
	    
	    //click on view FFT button displayed on OV Comment window
	  // webutils.driver.findElement(By.xpath("//*[@id='sensorBottomTabView:j_idt504']")).click();
	    
		
			}
	
	//Double click on class Name(any) it will display classification Section
	public static void doubleclickclassname() throws InterruptedException
	{
		Actions act = new Actions(webutils.driver);
	    act.doubleClick(webutils.driver.findElement(By.xpath("//*[@id='sensorBottomTabView:alarmStatusDataTable_data']/tr[1]/td[2]/a"))).perform();
	    Thread.sleep(5000);
	    scrolldown();
		
	}
	
	
	
	//Go to Results on Events page, navigate to the equipment
	public static void navigateResults(String plantName, String bldgName, String equipName) {
		
		webutils.driver.findElement(By.xpath("//form[@id='pdmTreeFrm']//span[text()='" +plantName+ "']/ancestor::li//span[text()='" +bldgName+ "']/ancestor::li//span[text()='" +equipName+ "']")).click();
	}
	
	//Select a neuron & go to PDM view
	public static void clickonaNeuron(String rownum) {

		webutils.driver.findElement(By.xpath("//tbody[@id='alaramTabView:sensorAlarms_data']//a[contains(@id, 'alaramTabView:sensorAlarms:"+rownum+"')]")).click();
		
		myWaitVar = new WebDriverWait(webutils.driver, 15);
		myWaitVar.until(ExpectedConditions.titleIs("PDM View"));
	}
    /**	
	//Click on Sensor on Equipment image
	public static void clickonSensoronEquipImg() {
		
		webutils.driver.findElement(By.xpath("//*[@id='equipmentImg']//*[name()='circle']")).click();
	}
	**/
	//OV Limit Graph
	public static void ovlimitGraph() {
		
		WebElement canvas = webutils.driver.findElement(By.xpath("//div[@class='ov-chart-min']//canvas"));
		new Actions(webutils.driver)
		 	//.moveToElement(canvas, 500, 30)
		.moveToElement(canvas,1137,60)
		 	.click().perform();
		
		webutils.driver.findElement(By.xpath("//div[@id='sensorBottomTabView:ovDetailsPanel']//a/span")).click();
	}
	
	//Classification
	public static void setClassification(String severity, String diagnosis, String confidence) throws InterruptedException {
		
		myWaitVar = new WebDriverWait(webutils.driver, 15);
		myWaitVar.until(ExpectedConditions.elementToBeClickable(By.id("sensorBottomTabView:severity_label")));
		
		//Severity: Select from dropdown other than"Unkown"
		webutils.driver.findElement(By.id("sensorBottomTabView:severity_label")).click();
		webutils.driver.findElement(By.xpath("//div[@id='sensorBottomTabView:severity_panel']//li[@data-label='"+severity+"']")).click();
		
		//Select diagnosis from diagnosis dropdown

		webutils.driver.findElement(By.xpath("//tbody[@id='sensorBottomTabView:reorderTable_data']//label[.='Select Diagnosis']")).click();
		webutils.driver.findElement(By.xpath("//div[contains(@id,'sensorBottomTabView:reorderTable:1:cols:3')]//li[@data-label='"+diagnosis+"']")).click();
		
		//Select analyst confidence from dropdown
		
		webutils.driver.findElement(By.id("sensorBottomTabView:reorderTable:3:cols:3:confidence_label")).click();
		webutils.driver.findElement(By.xpath("//div[@id='sensorBottomTabView:reorderTable:3:cols:3:confidence_panel']//li[@data-label='"+confidence+"']")).click();
		
		//Thread.sleep(5000);
		/**
		//Dragging & dropping the 4th column to the 1st column
		WebElement lastcolumn = webutils.driver.findElement(By.id("sensorBottomTabView:reorderTable:cols:3"));
		WebElement firstcolumn = webutils.driver.findElement(By.id("sensorBottomTabView:reorderTable:cols:0"));
		
		Actions builder = new Actions(webutils.driver);
		Action dragAndDrop = builder.clickAndHold(lastcolumn)
			.moveToElement(firstcolumn)
			    .release(firstcolumn)
		    .build();

		dragAndDrop.perform();
		**/
		
		//sensorBottomTabView:reorderTable:cols:0
		
		//Dragging & dropping the 4th column to the 1st column
				//WebElement lastcolumn = webutils.driver.findElement(By.id("sensorBottomTabView:reorderTable:cols:3"));
		WebElement lastcolumn = webutils.driver.findElement(By.xpath("//*[@id='sensorBottomTabView:reorderTable:cols:3']/span"));
				//WebElement firstcolumn = webutils.driver.findElement(By.id("sensorBottomTabView:reorderTable:cols:0"));
				WebElement firstcolumn = webutils.driver.findElement(By.xpath("//*[@id='sensorBottomTabView:reorderTable:cols:0']/span"));
				
				Actions builder = new Actions(webutils.driver);
				Action dragAndDrop = builder.clickAndHold(lastcolumn)
					.moveToElement(firstcolumn)
					.release(firstcolumn)
				    .build();

				dragAndDrop.perform();
						
				
		webutils.driver.findElement(By.xpath("//div[@id='sensorBottomTabView:classinfo']//span[text()='Save']")).click();
	}
	
	//FFT
	public static void setFFT(String option, String zfmin, String zfmax) {
		
		//Select the option to compare
		webutils.driver.findElement(By.id("sensorBottomTabView:fftCompareOptions_label")).click();
		webutils.driver.findElement(By.xpath("//div[@id='sensorBottomTabView:fftCompareOptions_panel']//li[@data-label='" +option+ "']")).click();
		webutils.driver.findElement(By.id("sensorBottomTabView:fftCompare")).click();
		
		//Set the Zoom Frequency
		webutils.driver.findElement(By.xpath("//div[@class='zoom-control']//input[@id='minX']")).sendKeys(zfmin);
		webutils.driver.findElement(By.xpath("//div[@class='zoom-control']//input[@id='maxX']")).sendKeys(zfmax);
		webutils.driver.findElement(By.xpath("//div[@class='zoom-control']//span[text()='Zoom Frequency']")).click();
		
		/**
		//Set the Zoom Amplitude
		webutils.driver.findElement(By.xpath("//div[@class='zoom-control']//input[@id='minY']")).sendKeys(zamin);
		webutils.driver.findElement(By.xpath("//div[@class='zoom-control']//input[@id='maxY']")).sendKeys(zamax);
		webutils.driver.findElement(By.xpath("//div[@class='zoom-control']//span[text()='Zoom Amplitude']")).click();
		**/
	}
	
	//Go to Add User 
	public static void gotoAddUser() {
		
		WebElement admin = webutils.driver.findElement(By.linkText("Admin"));

		Actions builder = new Actions(webutils.driver);
		Action mouseOverConfig = builder.moveToElement(admin).build();
		mouseOverConfig.perform();
		
		webutils.driver.findElement(By.linkText("User Accounts")).click();
	}
	
	//Click on Add User
	public static void clickonAddUser() {
		
		webutils.driver.findElement(By.xpath("//div[@class='page-title']//span[text()='Add User']")).click();
	}
	
	//Creating user accounts
	public static void addUser(String userName, String pwd, String confpwd, String roleName, String firstName, String lastName, String emailId) {
		
		webutils.driver.findElement(By.xpath("//form[@id='AddUserForm']//input[@id='userName']")).sendKeys(userName);
		webutils.driver.findElement(By.xpath("//form[@id='AddUserForm']//input[@id='userPassword']")).sendKeys(pwd);
		webutils.driver.findElement(By.xpath("//form[@id='AddUserForm']//input[@id='confirmPassword']")).sendKeys(confpwd);
		
		webutils.driver.findElement(By.xpath("//div[@id='role']/a/label")).click();
		if (roleName.equals(""))
			webutils.driver.findElement(By.xpath("//div[@id='role_panel']//a/span")).click();
		else
			webutils.driver.findElement(By.xpath("//div[@id='role_panel']//li[.='"+roleName+"']/label")).click();
		/**
		//Selects all the roles including "No Access" role
		List<WebElement> els = webutils.driver.findElements(By.xpath("//div[@id='role_panel']//li"));
		for ( WebElement el : els ) {
		    if ( !el.isSelected() ) {
		        el.click();
		    }
		}
		**/
		webutils.driver.findElement(By.xpath("//form[@id='AddUserForm']//input[@id='firstName']")).sendKeys(firstName);
		webutils.driver.findElement(By.xpath("//form[@id='AddUserForm']//input[@id='lastName']")).sendKeys(lastName);
		webutils.driver.findElement(By.xpath("//form[@id='AddUserForm']//input[@id='emailId']")).sendKeys(emailId);
	}
	
	//Click on CreateUser Save button
	public static void adduserSave() {
		
		webutils.driver.findElement(By.id("saveNewUser")).click();
	}
	
	//Click on CreateUser Save button
	public static void adduserCancel() {
		
		webutils.driver.findElement(By.id("resetform")).click();
	}
	
	//Select access control when the users are created
	public static void adduserAccessControl(String plant, String building, String system, String subsystem) {
		
		//Selecting the plant
		switch (plant) {
		case "All":
			webutils.driver.findElement(By.xpath("//div[@id='plantSelectAllId']//span[text()='Select All']")).click();
			break;
	    default:
			webutils.driver.findElement(By.xpath("//table[@id='plantsListId']//label[text()='"+plant+"']")).click();
	        break;
		}

		//Selecting the building
		switch (building) {
		case "All":
			webutils.driver.findElement(By.xpath("//div[@id='buildingSelectAllId']//span[text()='Select All']")).click();
			break;
	    default:
			webutils.driver.findElement(By.xpath("//table[@id='buildingsListId']//label[text()='"+building+"']")).click();
	        break;
		}
	
		//Selecting the system
		switch (system) {
		case "All":
			webutils.driver.findElement(By.xpath("//div[@id='systemSelectAllId']//span[text()='Select All']")).click();
			break;
	    default:
			webutils.driver.findElement(By.xpath("//table[@id='systemsListId']//label[text()='"+system+"']")).click();
	        break;
		}
		
		//Selecting the sub-system
		switch (subsystem) {
		case "All":
			webutils.driver.findElement(By.xpath("//div[@id='subSystemSelectAllId']//span[text()='Select All']")).click();
			break;
	    default:
			webutils.driver.findElement(By.xpath("//table[@id='subSystemsListId']//label[text()='"+subsystem+"']")).click();
	        break;
		}
	}
	
	//Admin role menu
	public static void adminroleMenu() {
		
		webutils.driver.findElement(By.xpath("//li[@class='active']//span[text()='Home']")).isDisplayed();
		webutils.driver.findElement(By.xpath("//li[@id='eventFilters']//span[text()='Events']")).isDisplayed();
		webutils.driver.findElement(By.xpath("//li[@class='drop-menu']//span[text()='Configure']")).isDisplayed();
		webutils.driver.findElement(By.xpath("//li[@class='drop-menu']//span[text()='Admin']")).isDisplayed();
		webutils.driver.findElement(By.xpath("//li[@class='drop-menu']//span[text()='Help']")).isDisplayed();
		webutils.driver.findElement(By.xpath("//ul[@class='account-link']//span")).isDisplayed();
	}
	
	//Planner role menu
	public static void plannerroleMenu() {

		webutils.driver.findElement(By.xpath("//li[@class='active']//span[text()='Home']")).isDisplayed();
		webutils.driver.findElement(By.xpath("//li[@id='eventFilters']//span[text()='Events']")).isDisplayed();
		webutils.driver.findElement(By.xpath("//li[@class='drop-menu']//span[text()='Configure']")).isDisplayed();
		webutils.driver.findElement(By.xpath("//li[@class='drop-menu']//span[text()='Help']")).isDisplayed();
		webutils.driver.findElement(By.xpath("//ul[@class='account-link']//span")).isDisplayed();
	}
	
	//Analyst role menu
	public static void analystroleMenu() {
		
		webutils.driver.findElement(By.xpath("//li[@class='active']//span[text()='Home']")).isDisplayed();
		webutils.driver.findElement(By.xpath("//li[@id='eventFilters']//span[text()='Events']")).isDisplayed();
		webutils.driver.findElement(By.xpath("//li[@class='drop-menu']//span[text()='Help']")).isDisplayed();
		webutils.driver.findElement(By.xpath("//ul[@class='account-link']//span")).isDisplayed();
	}
	
	//Operator role menu
	public static void operatorroleMenu() {
		
		webutils.driver.findElement(By.xpath("//li[@class='active']//span[text()='Home']")).isDisplayed();
		webutils.driver.findElement(By.xpath("//li[@id='eventFilters']//span[text()='Events']")).isDisplayed();
		webutils.driver.findElement(By.xpath("//li[@class='drop-menu']//span[text()='Help']")).isDisplayed();
		webutils.driver.findElement(By.xpath("//ul[@class='account-link']//span")).isDisplayed();
	}
	
	//click on Frequencyitems Button
	public static void frequencybutton()
	{
	 webutils.driver.findElement(By.xpath("//*[@id='hierarchyTabView']/ul/li[2]")).click();
	 //webutils.driver.findElement(By.xpath("//div[@id='selectedPlantId_panel']//li[@data-label='"+plantName+"']")).click();
		
	}
	//Select Frequencyitems on Dropdown
	public static void selectfrequency(String frequencyitem)
	{
		 //Click on drop down button
		webutils.driver.findElement(By.id("hierarchyTabView:j_idt773_label")).click();
		
		// Select Frequencyitems from Dropdown 
		webutils.driver.findElement(By.xpath("//div[@id='hierarchyTabView:j_idt773_panel']//div//ul//li[@data-label='"+frequencyitem+"']")).click();
		
		//Click on Add Button
		webutils.driver.findElement(By.xpath("//*[@id='hierarchyTabView:j_idt776']")).click();
		//webutils.driver.findElement(By.xpath("//button[@id='hierarchyTabView:j_idt776']//span[text()='Add']")).click();
		
	}
	
	public static void addBearing(String make, String model, String speedinput, String activeposition) throws InterruptedException
	{
		Thread.sleep(3000);
		
		focusthelatestwindow();
		
		webutils.driver.findElement(By.xpath("//*[@id='bearingPopup_title']")).click();
		
		          //Click on make dropdown
				webutils.driver.findElement(By.xpath("//*[@id='popups:j_idt561_label']")).click();
				
				// Select make dropdown value
				webutils.driver.findElement(By.xpath("//div[@id='popups:j_idt561_panel']//div//ul//li[@data-label='"+make+"']")).click();
				webutils.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
				Thread.sleep(2000);
				try {
				//Click on Model dropdown
				webutils.driver.findElement(By.xpath("//*[@id='popups:modelID_label']")).click();
				
				//Select modle dropdown value
				webutils.driver.findElement(By.xpath("//div[@id='popups:modelID_panel']//div//ul//li[@data-label='"+model+"']")).click();
				}
				catch(ElementNotVisibleException el)
				{
					el.toString();
				}
				webutils.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
				Thread.sleep(2000);
				
				//Click Speedinput dropdown
				webutils.driver.findElement(By.xpath("//*[@id='popups:speedInputValID_label']")).click();
				
				//Select SpeedInput value
				webutils.driver.findElement(By.xpath("//div[@id='popups:speedInputValID_panel']//div//ul//li[@data-label='"+speedinput+"']")).click();
				webutils.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
				Thread.sleep(2000);
								
				// Select Active position
				//webutils.driver.findElement(By.xpath("//*[@id='popups:position']/tbody/tr/td[1]/div/div[2]")).click();
				webutils.driver.findElement(By.xpath("//form[@id='popups']//label[text()='"+activeposition+"']")).click();
				Thread.sleep(2000);
				
				
				//Click on Save button
				webutils.driver.findElement(By.xpath("//form[@id='popups']//span[text()='Save']")).click();
				webutils.driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
					
		
	}
	public static void addgearboxTemplate(String name, String inputspeed, String drivesideteeth1, String drivesideteeth2, String gearactposition) throws InterruptedException
	{
          Thread.sleep(3000);
		
		focusthelatestwindow();
		webutils.driver.findElement(By.xpath("//*[@id='gearPopup_title']")).click();
		//Enter name
		webutils.driver.findElement(By.xpath("//*[@id='gearForm:j_idt586']")).sendKeys(name);
		//Select Inputspeed
		webutils.driver.findElement(By.xpath("//*[@id='gearForm:j_idt588_label']")).click();
		//Select Inputspeed value
		webutils.driver.findElement(By.xpath("//div[@id='@id='gearForm:j_idt588_panel']//div//ul//li[@data-label='"+inputspeed+"']")).click();
		webutils.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		Thread.sleep(2000);
		// Enter drive side speed one
		 webutils.driver.findElement(By.xpath("//*[@id='gearForm:j_idt592']")).sendKeys(drivesideteeth1);
		 //Enter drive side speed two
		 webutils.driver.findElement(By.xpath("//*[@id='gearForm:j_idt595']")).sendKeys(drivesideteeth2);
		 
		// Select Active position
			//webutils.driver.findElement(By.xpath("//*[@id='popups:position']/tbody/tr/td[1]/div/div[2]")).click();
			webutils.driver.findElement(By.xpath("//form[@id='gearForm']//label[text()='"+gearactposition+"']")).click();
			Thread.sleep(2000);
			
			
			//Click on Save button
			webutils.driver.findElement(By.xpath("//form[@id='gearForm']//span[text()='Save']")).click();
			webutils.driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
				
		
		
		
		
	}
	
	
	
	
}
