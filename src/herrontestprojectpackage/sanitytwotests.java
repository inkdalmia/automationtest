package herrontestprojectpackage;

import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

public class sanitytwotests {
	
	//Equipment for PDM view
	String plantName = "NBI lab", bldgName = "NBI test bench", equipName = "Oct15Test01";
	
	//FFT compare options
	String option = "Avg FFT [all known events where equipment was on]", zfmin = "0", zfmax = "20000";
	
	//Classification settings
	String severity = "Critical", diagnosis = "MD03 - Stage 3 Bearing Defect", confidence = "Confirmed";
	
	WebElement ele;
	
	//Path to store the screenshots
	String filepath = "C:\\Users\\praminen\\workspace\\HerronTestProject";
	
	@Test(priority=0)
	public void initialstep() throws Exception {
		
		webutils.launchFFBrowser();

		webutils.navigatetoURL();
		
		webutils.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		webutils.driver.manage().timeouts().setScriptTimeout(180, TimeUnit.SECONDS);
	
		//Login as Analyst
		webutils.signinAs("analyst", "pass@123");
		Assert.assertEquals("Dashboard", webutils.driver.getTitle());
	}
	
	@Test(priority=1)
	public void createnewprivatefilter() throws Exception {
		
		herrontestclass.gotoCreateNewPrivateFilter();
		herrontestclass.createNewFilter(plantName);
	}
	
	@Test(priority=2)
	public void gotopdmview() throws Exception {
		
		herrontestclass.navigateResults(plantName, bldgName, equipName);
		herrontestclass.clickonaNeuron("0");		
		Assert.assertEquals("PDM View", webutils.driver.getTitle());
	}
	
	@Test(priority=3)
	public void ovlimit() throws Exception {
		
		herrontestclass.ovlimitGraph();
		webutils.takeaScreenShot(filepath+"\\OVlimit.jpg");
	}
		
	@Test(priority=4)
	public void FFTcompare() throws Exception {
		
		herrontestclass.setFFT(option, zfmin, zfmax);
		webutils.takeaScreenShot(filepath+"\\FFT.jpg");
	}

	@Test(priority=5)
	public void classification() throws Exception {
		
		herrontestclass.setClassification(severity, diagnosis, confidence);
		webutils.takeaScreenShot(filepath+"\\Classification.jpg");
		ele = webutils.driver.findElement(By.xpath("//*[@id='messages_container']/div/div/div/span"));
		Assert.assertEquals("Class details updated successfully.", ele.getText());
	}
		
	@Test(priority=6)
	public void logout() throws Exception {
		
		webutils.logOut();
		Assert.assertEquals("Stafford Herron", webutils.driver.getTitle());
	}
	
	@AfterTest
	public void shutdown() throws Exception {
			
		webutils.terminate();
	}
}
