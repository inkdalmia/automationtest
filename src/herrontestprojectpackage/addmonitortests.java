package herrontestprojectpackage;

import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

public class addmonitortests {
	
	String monitor = "PR Mon2", serialno = "2", macadd = "00000002", connectivity = "Eth", ipoption = "DHCP";
			
	@Test(priority=0)
	public void initialstep() throws Exception {
			
		webutils.launchFFBrowser();

		webutils.navigatetoURL();
			
		webutils.driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		webutils.driver.manage().timeouts().setScriptTimeout(120, TimeUnit.SECONDS);
			
		webutils.signinAs("admin", "pass@123");
		Assert.assertEquals("Dashboard", webutils.driver.getTitle());
	}
		
	@Test(priority=1)
	public void addmonitor() throws Exception {
			
		herrontestclass.gotoEquipmentSetup();
		herrontestclass.clickonAddMonitor();
		
		herrontestclass.addMonitor(monitor, serialno, macadd, connectivity, ipoption);
			
		webutils.driver.navigate().refresh();
			
		Assert.assertEquals(webutils.driver.getPageSource().contains(monitor), true);
	}
	
	//need to add the -ve scenarios
		
	@AfterTest
	public void shutdown() throws Exception {
			
		webutils.terminate();
	}
}
