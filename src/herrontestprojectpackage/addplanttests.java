package herrontestprojectpackage;

import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

public class addplanttests {
	
	String plant = "Test2";
	WebElement ele;
		
	@Test(priority=0)
	public void initialstep() throws Exception {
		
		webutils.launchFFBrowser();

		webutils.navigatetoURL();
		
		webutils.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		webutils.driver.manage().timeouts().setScriptTimeout(60, TimeUnit.SECONDS);
		
		webutils.signinAs("admin", "pass@123");
		Assert.assertEquals("Dashboard", webutils.driver.getTitle());
	}
	
	@Test(priority=1)
	public void addplant() throws Exception {
		
		herrontestclass.addPlant(plant);
		
		webutils.driver.navigate().refresh();
		
		Assert.assertEquals(true, webutils.driver.getPageSource().contains(plant));
	}
	
	@Test(priority=2)
	public void addplantDupPlant() throws Exception {

		herrontestclass.addPlant(plant);

		ele = webutils.driver.findElement(By.xpath("//*[@id='messages_container']/div/div/div/span"));
		Assert.assertEquals("Duplicate Plant. Please use different name.", ele.getText());
	}
	
	@Test(priority=3)
	public void addplantNoPlant() throws Exception {
		
		herrontestclass.addplantClear();
		herrontestclass.addplantCancel();

		herrontestclass.addPlant("");
		
		ele = webutils.driver.findElement(By.xpath("//*[@id='messages_container']/div/div/div/span"));
		Assert.assertEquals("Please Enter Plant Name.", ele.getText());
	}
	
	@AfterTest
	public void shutdown() throws Exception {
		
		webutils.terminate();
	}
}
