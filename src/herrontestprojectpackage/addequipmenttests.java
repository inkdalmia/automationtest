package herrontestprojectpackage;

import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

public class addequipmenttests {

	String plant = "Hawthorn Farm", building = "HF-1", equipname = "Equip2", equipid = "7002";
	WebElement ele;
	
	@Test(priority=0)
	public void initialstep() throws Exception {
		
		webutils.launchFFBrowser();

		webutils.navigatetoURL();
		
		webutils.driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		webutils.driver.manage().timeouts().setScriptTimeout(300, TimeUnit.SECONDS);
		
		webutils.signinAs("admin", "pass@123");
		Assert.assertEquals("Dashboard", webutils.driver.getTitle());
	}
	
	@Test(priority=1)
	public void addequipment() throws Exception {
		
		herrontestclass.addEquipment(plant, building, equipname, equipid);
		
		webutils.driver.navigate().refresh();
		
		Assert.assertEquals(webutils.driver.getPageSource().contains(equipname), true);
	}
	
	@Test(priority=2)
	public void addequipmentDupEquipName() throws Exception {

		herrontestclass.addEquipment(plant, building, equipname, equipid);
		
		ele = webutils.driver.findElement(By.xpath("//*[@id='messages_container']/div/div/div/span"));
		Assert.assertEquals("Equipment Name already exists.", ele.getText());
		//Assert.assertEquals(true, webutils.driver.getPageSource().contains("Equipment Name already exists."));
	}
	
	@Test(priority=3)
	public void addequipmentDupEquipID() throws Exception {
		String equipname = "Equip100";
		herrontestclass.addEquipment(plant, building, equipname, equipid);
		
		ele = webutils.driver.findElement(By.xpath("//*[@id='messages_container']/div/div/div/span"));
		Assert.assertEquals("Equipment id already exists.", ele.getText());
		//Assert.assertEquals(true, webutils.driver.getPageSource().contains("Equipment id already exists."));
	}
	
	@Test(priority=4)
	public void addequipmentNoPlant() throws Exception {

		herrontestclass.addEquipment("Select Plant", building, equipname, equipid);
		
		ele = webutils.driver.findElement(By.xpath("//*[@id='messages_container']/div/div/div/span"));
		Assert.assertEquals("Please select Plant.", ele.getText());
		//Assert.assertEquals(true, webutils.driver.getPageSource().contains("Please select Plant."));
	}
	
	@Test(priority=5)
	public void addequipmentNoBuilding() throws Exception {

		herrontestclass.addEquipment(plant, "Select Building", equipname, equipid);
		
		ele = webutils.driver.findElement(By.xpath("//*[@id='messages_container']/div/div/div/span"));
		Assert.assertEquals("Please select Building.", ele.getText());
		//Assert.assertEquals(true, webutils.driver.getPageSource().contains("Please select Building."));
	}
	
	@Test(priority=6)
	public void addequipmentNoPlantBuilding() throws Exception {

		herrontestclass.addEquipment("Select Plant", "Select Building", equipname, equipid);
		
		ele = webutils.driver.findElement(By.xpath("//*[@id='messages_container']/div/div/div/span"));
		Assert.assertEquals("Please select Plant.", ele.getText());
		//Assert.assertEquals(true, webutils.driver.getPageSource().contains("Please select Plant."));
	}
	
	@Test(priority=7)
	public void addequipmentNoEquipName() throws Exception {

		herrontestclass.addEquipment(plant, building, "", equipid);
		
		ele = webutils.driver.findElement(By.xpath("//*[@id='messages_container']/div/div/div/span"));
		Assert.assertEquals("Equipment Name is Required.", ele.getText());
		//Assert.assertEquals(true, webutils.driver.getPageSource().contains("Equipment Name is Required."));
	}
	
	@Test(priority=8)
	public void addequipmentNoEquipID() throws Exception {

		herrontestclass.addEquipment(plant, building, equipname, "");
		
		ele = webutils.driver.findElement(By.xpath("//*[@id='messages_container']/div/div/div/span"));
		Assert.assertEquals("Equipment ID is Required.", ele.getText());
		//Assert.assertEquals(true, webutils.driver.getPageSource().contains("Equipment ID is Required."));
	}
	
	@AfterTest
	public void shutdown() throws Exception {
		
		webutils.terminate();
	}
}


