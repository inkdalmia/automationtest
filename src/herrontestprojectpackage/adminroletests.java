package herrontestprojectpackage;

import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

public class adminroletests {

	//Add user info
	String username = "user06", pwd = "pass@123", confpwd = "pass@123", rolename = "Planner", firstname = "firstname", lastname = "lastname", emailid = "first06.last@yahoo.com";
	
	@Test(priority=0)
	public void initialstep() throws Exception {
		
		webutils.launchFFBrowser();

		webutils.navigatetoURL();
		
		webutils.driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		webutils.driver.manage().timeouts().setScriptTimeout(180, TimeUnit.SECONDS);
		
		webutils.signinAs("admin", "pass@123");
		Assert.assertEquals("Dashboard", webutils.driver.getTitle());
	}
	
	@Test(priority=1)
	public void verifyadminmenu() throws Exception {
		
		herrontestclass.adminroleMenu();
	}
	
	@Test(priority=2)
	public void adduser() throws Exception {
		
		herrontestclass.gotoAddUser();
		herrontestclass.addUser(username, pwd, confpwd, rolename, firstname, lastname, emailid);
		herrontestclass.adduserSave();
		
		webutils.driver.navigate().refresh();
		
		Assert.assertEquals(true, webutils.driver.getPageSource().contains(username));
	}
	
	@Test(priority=13)
	public void logout() throws Exception {
		
		webutils.logOut();
		Assert.assertEquals("Stafford Herron", webutils.driver.getTitle());
	}
	
	@AfterTest
	public void shutdown() throws Exception {
			
		webutils.terminate();
	}
}
