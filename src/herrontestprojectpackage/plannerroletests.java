package herrontestprojectpackage;

import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.NoSuchElementException;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

public class plannerroletests {

	@Test(priority=0)
	public void initialstep() throws Exception {
		
		webutils.launchFFBrowser();

		webutils.navigatetoURL();
		
		webutils.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		webutils.driver.manage().timeouts().setScriptTimeout(240, TimeUnit.SECONDS);
		
		webutils.signinAs("planner", "pass@123");
		Assert.assertEquals("PDM Configurations", webutils.driver.getTitle());
	}
	
	@Test(priority=1)
	public void verifyplannermenu() throws Exception {
		
		herrontestclass.plannerroleMenu();
	}
	
	@Test(priority=2,expectedExceptions=NoSuchElementException.class)
	public void gotoadminmenu() throws Exception {
		
		herrontestclass.gotoAddUser();
	}
	
	@Test(priority=13)
	public void logout() throws Exception {
		
		webutils.logOut();
		Assert.assertEquals("Stafford Herron", webutils.driver.getTitle());
	}
	
	@AfterTest
	public void shutdown() throws Exception {
			
		webutils.terminate();
	}
}
