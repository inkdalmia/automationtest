package herrontestprojectpackage;

import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

public class sanitytest1 {
	
    //Plant Name
	String plant = "Test7";
	
	//Classification Setting
	
	String severity="Unknown",diagnosis="MD02 - Stage 2 Bearing Defect",confidence="Medium";
	WebElement ele;
	//Screen Shots location
	 String filepath="C:\\HerronTestProject";
	
	@Test(priority=0)
	public void loginasAnalyst() throws Exception
	{
		//webutils.launchFFBrowser();
				webutils.launchChromeBrowser();
				webutils.navigatetoURL();
				
				webutils.driver.manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);
				webutils.driver.manage().timeouts().setScriptTimeout(240, TimeUnit.SECONDS);
		        webutils.singinAsanalyst("analyst", "pass@123");
		
		
	}

	@Test(priority=1)
	public void addplantonanalyst() throws Exception
	{
		herrontestclass.gotoCreateNewPublicFilter();
		herrontestclass.createNewFilter(plant);
		herrontestclass.doubleclickonequipment();
		herrontestclass.scrolldownofanalyst();
		
		
			}
	
	
	@Test(priority=2)
	public void classfication() throws Exception
	{
		herrontestclass.doubleclickclassname();
		//FFTDetails ScreenShot
		webutils.takeaScreenShot(filepath+"\\FFT.jpg");
		
				herrontestclass.scrolldownofanalyst();
				
				
				
		 herrontestclass.setClassification(severity, diagnosis, confidence);
		 
		 // Fourth column change to first column ScreenShot
		 webutils.takeaScreenShot(filepath+"\\classification.jpg");
	}
	
	@Test(priority=4)
	public void logout() throws Exception
	{
		webutils.logOut();
		Assert.assertEquals("Stafford Herron", webutils.driver.getTitle());
		
	}
	@Test(priority=5)
	public void closeBrowser() throws Exception
	{
		// Close the Browser
		webutils.driver.quit();
		
	}
}
		
		