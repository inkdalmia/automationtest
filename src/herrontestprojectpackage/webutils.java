package herrontestprojectpackage;

import java.io.File;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.apache.commons.io.FileUtils;


public class webutils {
	static WebDriver driver;
	static String url = "http://10.55.215.215:8080/HerronUI/";

	//Launch FireFox browser
	public static void launchFFBrowser() throws Exception {
				
		driver = new FirefoxDriver();
		//driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}
	
	//Launch IE browser
	public static void launchIEBrowser() throws Exception {
		
		System.setProperty("webdriver.ie.driver", "C:\\Users\\praminen\\Documents\\Selenium\\IEDriverServer.exe");
		
		DesiredCapabilities caps = DesiredCapabilities.internetExplorer();
		caps.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,true);
		driver = new InternetExplorerDriver(caps);
		//driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}
	
	//Launch Chrome browser
	public static void launchChromeBrowser() throws Exception {
		
		System.setProperty("webdriver.chrome.driver", "D:\\Installables\\Selenium_Instalaltion\\chromedriver.exe");
		driver = new ChromeDriver();
	   // driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

	}
	
	//Navigate to the corresponding page
	public static void navigatetoURL() {
		
		driver.navigate().to(url);
	}
	
	// Login to Stafford 
	public static void signinAs(String username, String password) throws Exception {
		  
		driver.findElement(By.xpath("//div[@class='navigation-bar']//a")).click();
		
		driver.findElement(By.id("loginForm:user")).clear();
	    driver.findElement(By.id("loginForm:user")).sendKeys(username);
	    
	    driver.findElement(By.id("loginForm:password")).clear();
	    driver.findElement(By.id("loginForm:password")).sendKeys(password);
	    
	    driver.findElement(By.id("loginForm:loginButton")).click();
	}
	
	// Login as analyst
	public static void singinAsanalyst(String username1,String password1)
	{
       driver.findElement(By.xpath("//div[@class='navigation-bar']//a")).click();
		
		driver.findElement(By.id("loginForm:user")).clear();
	    driver.findElement(By.id("loginForm:user")).sendKeys(username1);
	    
	    driver.findElement(By.id("loginForm:password")).clear();
	    driver.findElement(By.id("loginForm:password")).sendKeys(password1);
	    
	    driver.findElement(By.id("loginForm:loginButton")).click();
		
	}
	
	//Convert RGB to HEX
	public static String rgbtoHex(String color) {
		
		String hex;
    	String[] numbers = color.replace("rgba(", "").replace(")", "").split(",");
    	int r = Integer.parseInt(numbers[0].trim());
    	int g = Integer.parseInt(numbers[1].trim());
    	int b = Integer.parseInt(numbers[2].trim());

    	hex = "#" + Integer.toHexString(r) + Integer.toHexString(g) + Integer.toHexString(b);

    	return hex;
	}
	
	//Take a screenshot
	public static void takeaScreenShot(String filepath) throws Exception {
		
		File screenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(screenshot, new File(filepath));	
	}
	
	//Logout
	public static void logOut() {

		WebElement logout = webutils.driver.findElement(By.xpath("//ul[@class='account-link']//span"));
		
		Actions builder = new Actions(webutils.driver);
		Action mouseOverConfig = builder.moveToElement(logout).build();
		mouseOverConfig.perform();
		
		webutils.driver.findElement(By.linkText("Logout")).click();
	}
	
	//Close the driver & exit
	public static void terminate() throws Exception {
		
		driver.quit();
		System.exit(0);
	}
}
