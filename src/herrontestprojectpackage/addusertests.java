package herrontestprojectpackage;

import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

public class addusertests {

	WebElement ele;
	
	//Add user info
	String username = "user06", pwd = "pass@123", confpwd = "pass@123", rolename = "Planner", firstname = "firstname", lastname = "lastname", emailid = "first06.last@yahoo.com";
	
	//Add user access control info
	String plant = "Hawthorn Farm", building = "HF-1", system = "Chill Water", subsystem = "Chillers";
	
	@Test(priority=0)
	public void initialstep() throws Exception {
		
		webutils.launchFFBrowser();

		webutils.navigatetoURL();
		
		webutils.driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		webutils.driver.manage().timeouts().setScriptTimeout(180, TimeUnit.SECONDS);
		
		webutils.signinAs("admin", "pass@123");
		Assert.assertEquals("Dashboard", webutils.driver.getTitle());
	}

	@Test(priority=1)
	public void adduser() throws Exception {
		
		herrontestclass.gotoAddUser();
		herrontestclass.clickonAddUser();
		herrontestclass.addUser(username, pwd, confpwd, rolename, firstname, lastname, emailid);
		//herrontestclass.adduserAccessControl(plant, building, system, subsystem);
		herrontestclass.adduserSave();
		
		webutils.driver.navigate().refresh();
		
		Assert.assertEquals(true, webutils.driver.getPageSource().contains(username));
	}
	
	@Test(priority=2)
	public void dupuser() throws Exception {
		
		herrontestclass.gotoAddUser();
		herrontestclass.clickonAddUser();
		herrontestclass.addUser(username, pwd, confpwd, rolename, firstname, lastname, emailid);
		herrontestclass.adduserSave();
		
		ele = webutils.driver.findElement(By.xpath("//*[@id='messages_container']/div/div/div/span"));
		Assert.assertEquals("User Id Exist. Please enter other User Id", ele.getText());
	}
	
	@Test(priority=3)
	public void dupemailid() throws Exception {
		String username = "user100";
		
		herrontestclass.gotoAddUser();
		herrontestclass.clickonAddUser();
		herrontestclass.addUser(username, pwd, confpwd, rolename, firstname, lastname, emailid);
		herrontestclass.adduserSave();
		
		ele = webutils.driver.findElement(By.xpath("//*[@id='messages_container']/div/div/div/span"));
		Assert.assertEquals("EmailId " +emailid+ " exists. Please choose another EmailId", ele.getText());
	}
	
	@Test(priority=4)
	public void nouserid() throws Exception {
		
		herrontestclass.gotoAddUser();
		herrontestclass.clickonAddUser();
		herrontestclass.addUser("", pwd, confpwd, rolename, firstname, lastname, emailid);
		herrontestclass.adduserSave();
		
		ele = webutils.driver.findElement(By.xpath("//*[@id='messages_container']/div/div/div/span"));
		Assert.assertEquals("User Id is required.", ele.getText());
	}
	
	@Test(priority=5)
	public void nopassword() throws Exception {
		
		herrontestclass.gotoAddUser();
		herrontestclass.clickonAddUser();
		herrontestclass.addUser(username, "", confpwd, rolename, firstname, lastname, emailid);
		herrontestclass.adduserSave();
		
		ele = webutils.driver.findElement(By.xpath("//*[@id='messages_container']/div/div/div/span"));
		Assert.assertEquals("Password is required", ele.getText());
	}
	
	@Test(priority=6)
	public void noconfpassword() throws Exception {
		
		herrontestclass.gotoAddUser();
		herrontestclass.clickonAddUser();
		herrontestclass.addUser(username, pwd, "", rolename, firstname, lastname, emailid);
		herrontestclass.adduserSave();
		
		ele = webutils.driver.findElement(By.xpath("//*[@id='messages_container']/div/div/div/span"));
		Assert.assertEquals("New password and confirm password doesnt match", ele.getText());
	}
	
	@Test(priority=7)
	public void norolename() throws Exception {
		
		herrontestclass.gotoAddUser();
		herrontestclass.clickonAddUser();
		herrontestclass.addUser(username, pwd, confpwd, "", firstname, lastname, emailid);
		herrontestclass.adduserSave();
		
		ele = webutils.driver.findElement(By.xpath("//*[@id='messages_container']/div/div/div/span"));
		Assert.assertEquals("Please select role for user.", ele.getText());		
	}
	
	@Test(priority=8)
	public void nofirstname() throws Exception {
		
		herrontestclass.gotoAddUser();
		herrontestclass.clickonAddUser();
		herrontestclass.addUser(username, pwd, confpwd, rolename, "", lastname, emailid);
		herrontestclass.adduserSave();
		
		ele = webutils.driver.findElement(By.xpath("//*[@id='messages_container']/div/div/div/span"));
		Assert.assertEquals("First Name is required.", ele.getText());
	}
	
	@Test(priority=9)
	public void nolastname() throws Exception {
		
		herrontestclass.gotoAddUser();
		herrontestclass.clickonAddUser();
		herrontestclass.addUser(username, pwd, confpwd, rolename, firstname, "", emailid);
		herrontestclass.adduserSave();
		
		ele = webutils.driver.findElement(By.xpath("//*[@id='messages_container']/div/div/div/span"));
		Assert.assertEquals("Last Name is required.", ele.getText());
	}
	
	@Test(priority=10)
	public void noemail() throws Exception {
		
		herrontestclass.gotoAddUser();
		herrontestclass.clickonAddUser();
		herrontestclass.addUser(username, pwd, confpwd, rolename, firstname, lastname, "");
		herrontestclass.adduserSave();
		
		ele = webutils.driver.findElement(By.xpath("//*[@id='messages_container']/div/div/div/span"));
		Assert.assertEquals("Email wrong format and is required.", ele.getText());
	}
	
	@Test(priority=11)
	public void invalidemailformat() throws Exception {
		String emailid = "first.last@";
		
		herrontestclass.gotoAddUser();
		herrontestclass.clickonAddUser();
		herrontestclass.addUser(username, pwd, confpwd, rolename, firstname, lastname, emailid);
		herrontestclass.adduserSave();
		
		ele = webutils.driver.findElement(By.xpath("//*[@id='messages_container']/div/div/div/span"));
		Assert.assertEquals("Invalid email format", ele.getText());
	}

	@Test(priority=13)
	public void logout() throws Exception {
		
		webutils.logOut();
		Assert.assertEquals("Stafford Herron", webutils.driver.getTitle());
	}
	
	@AfterTest
	public void shutdown() throws Exception {
			
		webutils.terminate();
	}
}
