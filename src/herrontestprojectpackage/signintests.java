package herrontestprojectpackage;

import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class signintests {
	
	@BeforeTest
	public void initialstep() throws Exception {
		
		webutils.launchFFBrowser();

		webutils.navigatetoURL();
		
		webutils.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		webutils.driver.manage().timeouts().setScriptTimeout(60, TimeUnit.SECONDS);
	}

	@Test(priority=0)
	public void invaliduser() throws Exception {
		
		webutils.signinAs("test", "pass@123");

		WebElement ele = webutils.driver.findElement(By.xpath("//*[@id='loginForm:growl_container']/div/div/div/span"));
		Assert.assertEquals("Incorrect user credentials, please try again.", ele.getText());
	}
	
	@Test(priority=1)
	public void nousername() throws Exception {
		
		webutils.signinAs("", "pass@123");
		WebElement ele = webutils.driver.findElement(By.xpath("//*[@id='loginForm:growl_container']/div/div/div/span"));
		Assert.assertEquals("Username is required.", ele.getText());
	}
	
	@Test(priority=2)
	public void nopassword() throws Exception {
		
		webutils.signinAs("praminen", "");
		WebElement ele = webutils.driver.findElement(By.xpath("//*[@id='loginForm:growl_container']/div/div/div/span"));
		Assert.assertEquals("Password is required.", ele.getText());
	}	

	@Test(priority=3)
	public void signin() throws Exception {
	
		webutils.signinAs("admin", "pass@123");
		Assert.assertEquals("Dashboard", webutils.driver.getTitle());
	}
	
	@Test(priority=4)
	public void logout() throws Exception {
		
		webutils.logOut();
		Assert.assertEquals("Stafford Herron", webutils.driver.getTitle());
	}
	
	@AfterTest
	public void shutdown() throws Exception {
		
		webutils.terminate();
	}
}
