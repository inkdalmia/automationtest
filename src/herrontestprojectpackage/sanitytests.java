package herrontestprojectpackage;

import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

public class sanitytests {

	//Set the data for Plant, Building, Equipment
	String plant = "Test7", building = "Building7", equipname = "Equip7", equipid = "70007";  
	//Set the data for adding a new monitor
	String monitor = "PR Mon7", serialno = "45807", macadd = "11_11_11_11_11_17", connectivity = "Eth", ipoption = "Static";
	//Set the data for Sensor Template
	String  make = "Make7", model = "Model7", sensitivity = "100", bias = "100";
	String tempname=make+"-"+model+"-"+sensitivity;
	//String tempname= "ACC Sensor";
	//Set the data for Sensor Serial Numbers
	String srn1="7", srn2="8";
	
	WebElement ele;
	
	@Test(priority=0)
	public void initialstep() throws Exception {
		
		//webutils.launchFFBrowser();
		webutils.launchChromeBrowser();
		
		webutils.navigatetoURL();
		
		webutils.driver.manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);
		webutils.driver.manage().timeouts().setScriptTimeout(240, TimeUnit.SECONDS);
		
		webutils.signinAs("planner", "pass@123");
		Assert.assertEquals("Stafford Herron", webutils.driver.getTitle());
	}

	@Test(priority=1)
	public void addplant() throws Exception {
		
		herrontestclass.addPlant(plant);
		
		//Verification		
		ele=webutils.driver.findElement(By.xpath("//*[@id='successMsgs']/div"));
		Assert.assertEquals("Plant added Successfully.", ele.getText());
						
			
	}
	
	@Test(priority=2)
	public void addbuilding() throws Exception {
		
		herrontestclass.addBuilding(plant, building);
		//Verification
		ele=webutils.driver.findElement(By.xpath("//*[@id='successMsgs']/div"));
		Assert.assertEquals("Building added Successfully.", ele.getText());
		
	}

	@Test(priority=3)
	public void addequipment() throws Exception {
		
		herrontestclass.addEquipment(plant, building, equipname, equipid);
		
		//Verification		
		ele=webutils.driver.findElement(By.xpath("//*[@id='successMsgs']/div"));
		Assert.assertEquals("Equipment Added Successfully.", ele.getText());
		
				
	}
	
	@Test(priority=4)
	public void addmonitor() throws Exception {
			
		//herrontestclass.gotoEquipmentSetup();
		//herrontestclass.clickonAddMonitor();
		
		herrontestclass.monitorandsensor();
		
		herrontestclass.addMonitor(monitor, serialno, macadd, connectivity, ipoption);
		
		//Verification	
		
		ele=webutils.driver.findElement(By.xpath("//*[@id='successMsgs']/div"));
		Assert.assertEquals("Monitor added succesfully", ele.getText());
		
		
		
	}
	
	@Test(priority=5)
	public void createSensor() throws Exception {
		
		herrontestclass.scrolldown();
		
		//herrontestclass.gotoSensorTemplate();
		herrontestclass.existingMonitor(monitor);
		//herrontestclass.clickonAddSensorTemplate();
		herrontestclass.sensorTemplate(make, model, sensitivity, bias);
		
		//Verification
		ele=webutils.driver.findElement(By.xpath("//*[@id='successMsgs']/div"));
		Assert.assertEquals("Sensor template added successfully", ele.getText());
		
	}
	
	@Test(priority=6)
	public void attachMonitor() throws Exception {
		
		herrontestclass.testTemplate(tempname, srn1);
		herrontestclass.addnewsensor(tempname, srn2);
		
		//Verification
		
		ele=webutils.driver.findElement(By.xpath("//*[@id='successMsgs']/div"));
		Assert.assertEquals("Sensor channel mapping successful.", ele.getText());
		
		
			}
	
	@Test(priority=7)
	public void logout() throws Exception {
		
		webutils.logOut();
		//Verification
		Assert.assertEquals("Stafford Herron", webutils.driver.getTitle());
	}
	
	@Test(priority=8)
	public void close() throws Exception
	{
		// close the browser
		webutils.driver.quit();
		
	}
	
	
	
	
	
		
}
